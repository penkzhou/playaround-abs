package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVStatus;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround_abs.app.MainActivity;
import com.penkzhou.playaround_abs.app.R;

/**
 * Created by Administrator on 14-2-26.
 */
public class PrivateTalkListFragment extends SherlockFragment implements View.OnClickListener {


    private final static String TAG = "PrivateTalkListFragment";
    private MainActivity mParent;
    private ListView talkList;
    private ImageButton sendButton;
    private EditText newtalkContent;
    private String toUserId;


    public PrivateTalkListFragment() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_conversation, container, false);
        toUserId = "532bb377e4b05651b105bd5e";
        sendButton = (ImageButton) rootView.findViewById(R.id.btn_talk_comment);
        newtalkContent = (EditText) rootView.findViewById(R.id.et_talk_content);
        sendButton.setOnClickListener(this);
        talkList = (ListView) rootView.findViewById(R.id.lv_talk_wordlist);
        return rootView;
    }

    public void loadTalkList() {

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_talk_comment:
                AVStatus status = new AVStatus();
                String content = newtalkContent.getText().toString();
                //Map<String,Object> talkContent = new HashMap<String, Object>();
                //talkContent.put("dest",toUserId);
                status.setMessage(content);
                //status.setData(talkContent);
                AVStatus.sendPrivateStatusInBackgroud(status, toUserId, new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e == null) {
                            Toast.makeText(getActivity(), "成功发送私信", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "发送私信fail", Toast.LENGTH_LONG).show();
                        }
                        Log.i(TAG, "Send private status finished.");
                    }
                });
                break;

        }
    }
}
