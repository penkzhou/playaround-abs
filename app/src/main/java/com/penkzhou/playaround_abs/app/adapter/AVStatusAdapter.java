package com.penkzhou.playaround_abs.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVStatus;
import com.avos.avoscloud.AVUser;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.util.TimeUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 14-1-17.
 */
public class AVStatusAdapter extends ArrayAdapter<AVStatus> {
    private Context activity;
    private List<AVStatus> objects;

    public AVStatusAdapter(Context context, int resource, List<AVStatus> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.weibolistitem, null);
            viewHolder = new ViewHolder();
            viewHolder.author = (TextView) v.findViewById(R.id.tvAuthorName);
            viewHolder.content = (TextView) v.findViewById(R.id.tvWeiboContent);
            viewHolder.weibotime = (TextView) v.findViewById(R.id.tvWeiboTime);
            viewHolder.avatarImg = (ImageView) v.findViewById(R.id.ivAvatarImg);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final AVStatus weiboStatus = objects.get(position);
        AVUser user = (AVUser) weiboStatus.getSource();
        AVFile avatar = (AVFile) user.get("avatarUrl");

        if (weiboStatus != null) {
            viewHolder.author.setText(((AVUser) weiboStatus.getSource()).getUsername());
            viewHolder.content.setText(weiboStatus.getMessage());
            viewHolder.weibotime.setText(TimeUtil.getTimeAgo(weiboStatus.getCreatedAt(), activity));
            Picasso.with(getContext()).load(avatar.getUrl()).placeholder(R.drawable.no_avatar).into(viewHolder.avatarImg);
        }
        return v;
    }

    public static class ViewHolder {
        public TextView author;
        public TextView weibotime;
        public TextView content;
        public ImageView avatarImg;

    }
}
