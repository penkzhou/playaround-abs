package com.penkzhou.playaround_abs.app.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;

import java.io.Serializable;

/**
 * Created by Administrator on 14-2-21.
 */
@AVClassName("Place")
public class Place extends AVObject implements Serializable {
    public final static String PLACELOCATION = "placeLocation";
    public final static String PLACENAME = "placeName";
    public final static String PLACELIKETIMES = "placeLikeTimes";
    public final static String PLACEVISITTIMES = "placeVisitTimes";
    public final static String PLACETYPE = "placeType";
    public final static String PLACEOWNER = "placeOwner";
    public final static String PLACEDESCRIBE = "placeDescribe";
    public final static String PLACEADDRESS = "placeAddress";
    public final static String PLACECOMMENTTIMES = "placeCommentTimes";


    public enum PlaceType {BASKETBALL, TABLETENNIS, TENNIS, FOOTBALL, BADMINTON, SNOOKER, VOLLEYBALL, RUNNING, SWIMMING, CLIMBING, ROCK_CLIMBING;}


    public Place() {

    }

    public AVGeoPoint getPlaceLocation() {
        return getAVGeoPoint(PLACELOCATION);
    }

    public void setPlaceLocation(AVGeoPoint location) {
        put(PLACELOCATION, location);
    }

    public String getPlaceName() {
        return getString(PLACENAME);
    }

    public void setPlaceName(String name) {
        put(PLACENAME, name);
    }

    public int getPlaceLikeTimes() {
        return getInt(PLACELIKETIMES);
    }

    public void setPlaceLikeTimes(int likeTimes) {
        put(PLACELIKETIMES, likeTimes);
    }

    public void incrementPlaceLikeTimes() {
        increment(PLACELIKETIMES);
    }

    public AVUser getPlaceOwner() {
        return getAVUser(PLACEOWNER);
    }

    public void setPlaceOwner(AVUser owner) {
        put(PLACEOWNER, owner);
    }

    public PlaceType getPlaceType() {
        int i = getInt(PLACETYPE);
        return PlaceType.values()[i];
    }

    public void setPlaceType(PlaceType type) {
        put(PLACETYPE, type.ordinal());
    }

    public int getPlaceVisitTimes() {
        return getInt(PLACEVISITTIMES);
    }

    public void setPlaceVisitTimes(int times) {
        put(PLACEVISITTIMES, times);
    }

    public void incrementPlaceVisitTimes() {
        increment(PLACEVISITTIMES);
    }

    public String getPlaceAddress() {
        return getString(PLACEADDRESS);
    }

    public void setPlaceAddress(String address) {
        put(PLACEADDRESS, address);
    }

    public String getPlaceDescribe() {
        return getString(PLACEDESCRIBE);
    }

    public void setPlaceDescribe(String describe) {
        put(PLACEDESCRIBE, describe);
    }

    public int getPlaceCommentTimes() {
        return getInt(PLACECOMMENTTIMES);
    }

    public void setPlaceCommentTimes(int times) {
        put(PLACECOMMENTTIMES, times);
    }

    public void incrementPlaceCommentTimes() {
        increment(PLACECOMMENTTIMES);
    }


}
