package com.penkzhou.playaround_abs.app.util;

/**
 * Created by Administrator on 14-2-27. from http://stackoverflow.com/questions/4677269/how-to-stretch-three-images-across-the-screen-preserving-aspect-ratio
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class AdjustDrawable extends ImageView {

    public AdjustDrawable(Context context) {
        super(context);
    }

    public AdjustDrawable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdjustDrawable(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
        setMeasuredDimension(width, height);
    }
}