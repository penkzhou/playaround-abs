package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.FollowCallback;
import com.avos.avoscloud.GetCallback;
import com.penkzhou.playaround_abs.app.OnProfileDataPass;
import com.penkzhou.playaround_abs.app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 14-3-1.
 */
public class ProfileFragment extends SherlockFragment implements View.OnClickListener {
    private ProgressDialog pdl;
    private TextView username, email, describe, following, fans, selfEditBtn, followBtn, followBtnOn, avatarTip;
    private ImageView avatar;
    private AVUser currentUser;
    private OnProfileDataPass dataPasser;
    private String userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("ProfileFragment", "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        username = (TextView) rootView.findViewById(R.id.tv_profile_username);
        email = (TextView) rootView.findViewById(R.id.tv_profile_email);
        describe = (TextView) rootView.findViewById(R.id.tv_profile_desc);
        following = (TextView) rootView.findViewById(R.id.tv_profile_follow);
        fans = (TextView) rootView.findViewById(R.id.tv_profile_fans);
        selfEditBtn = (TextView) rootView.findViewById(R.id.tv_profile_selfedit);
        followBtn = (TextView) rootView.findViewById(R.id.tv_profile_followbtn);
        followBtnOn = (TextView) rootView.findViewById(R.id.tv_profile_followbtn_on);
        avatarTip = (TextView) rootView.findViewById(R.id.tv_profile_avatar_tip);
        avatar = (ImageView) rootView.findViewById(R.id.iv_profile_avatar);
        followBtnOn.setOnClickListener(this);
        followBtn.setOnClickListener(this);
        fans.setOnClickListener(this);
        following.setOnClickListener(this);
        selfEditBtn.setOnClickListener(this);
        pdl = new ProgressDialog(getActivity());
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
        pdl.show();
        loadProfile();
        return rootView;
    }

    public void checkFollowRelative() {
        AVQuery<AVUser> followerNameQuery = currentUser.followerQuery(currentUser.getObjectId(), AVUser.class);
        followerNameQuery.whereEqualTo("follower", AVUser.getCurrentUser());

        followerNameQuery.findInBackground(new FindCallback<AVUser>() {
            @Override
            public void done(List<AVUser> avUsers, AVException e) {
                if (e == null) {
                    if (avUsers.size() > 0) { //说明已经关注了currentUser
                        followBtnOn.setVisibility(View.VISIBLE);
                    } else {
                        followBtn.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.d("checkFollowRelative error", "error code : " + e.getCode());
                }

            }
        });
    }

    public void setProfileInfo() {
        try {
            AVUser.followeeQuery(currentUser.getObjectId(), AVUser.class).countInBackground(new CountCallback() {
                @Override
                public void done(int i, AVException e) {
                    if (e == null) {
                        fans.setText(getText(R.string.profile_following).toString() + i);
                        if (i == 0) {
                            fans.setClickable(false);
                        }
                    } else {
                        Log.e("setProfileInfo_getFollowersInBackground", "error code : " + e.getCode());
                    }
                }
            });
            AVUser.followerQuery(currentUser.getObjectId(), AVUser.class).countInBackground(new CountCallback() {
                @Override
                public void done(int i, AVException e) {
                    if (e == null) {
                        following.setText(getText(R.string.profile_fans).toString() + i);
                        if (i == 0) {
                            following.setClickable(false);
                        }
                    } else {
                        //e.printStackTrace();
                        Log.e("setProfileInfo_getFollowersInBackground", "error code : " + e.getCode());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        AVFile avatarFile = (AVFile) currentUser.get("avatarUrl");
        if (avatarFile != null) {
            Picasso.with(getActivity()).load(avatarFile.getUrl()).into(avatar);
        } else {
            avatar.setImageResource(R.drawable.no_avatar);
        }
        username.setText(currentUser.getUsername());
        email.setText(currentUser.getEmail());
        describe.setText(currentUser.getString("intro"));
    }

    public void loadProfile() {
        Log.d("ProfileFragment", "loadProfile");
        if (userId.equals(AVUser.getCurrentUser().getObjectId())) {//进入自己的页面
            currentUser = AVUser.getCurrentUser();
            avatar.setOnClickListener(this);
            avatarTip.setVisibility(View.VISIBLE);
            setProfileInfo();
        } else {
            selfEditBtn.setVisibility(View.GONE);
            AVQuery<AVUser> query = AVUser.getQuery();
            query.whereEqualTo("objectId", userId);
            query.getFirstInBackground(new GetCallback<AVUser>() {
                @Override
                public void done(AVUser avUser, AVException e) {
                    if (e == null) {
                        currentUser = avUser;
                        Log.d("currentUser ", avUser.getEmail());
                        setProfileInfo();
                        checkFollowRelative();
                    } else {
                        e.printStackTrace();
                        Log.e("loadProfile 查询用户失败", e.getMessage());
                    }
                }
            });
            Log.d("ProfileFragment", userId);
        }
        pdl.dismiss();
    }


    @Override
    public void onAttach(Activity activity) {
        Log.d("ProfileFragment", "onAttach");
        super.onAttach(activity);
        try {
            dataPasser = (OnProfileDataPass) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnProfileDataPass");
        }
        userId = dataPasser.getCurrentUserId();
        Log.d("终于得到了 userId", userId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_profile_followbtn:
                AVUser.getCurrentUser().followInBackground(currentUser.getObjectId(), new FollowCallback() {
                    @Override
                    public void done(AVObject avObject, AVException e) {
                        if (e == null) {
                            followBtn.setVisibility(View.GONE);
                            followBtnOn.setVisibility(View.VISIBLE);
                        } else {
                            e.printStackTrace();
                            Log.e("tv_profile_followbtn 失败", e.getMessage());
                        }
                    }
                });
                break;
            case R.id.tv_profile_followbtn_on:

                break;
            case R.id.tv_profile_selfedit:
                Intent setting = new Intent(getActivity(), ApplicationSettingActivity.class);
                startActivity(setting);
                break;
            case R.id.tv_profile_fans:
                Intent toFollowee = new Intent(getActivity(), FolloweeUserListActivity.class);
                toFollowee.putExtra("userId", currentUser.getObjectId());
                startActivity(toFollowee);
                break;
            case R.id.tv_profile_follow:
                Intent toFollower = new Intent(getActivity(), FollowerUserListActivity.class);
                toFollower.putExtra("userId", currentUser.getObjectId());
                startActivity(toFollower);
                break;
            case R.id.iv_profile_avatar:
                Intent cropIntent = new Intent(getActivity(), CropAvatarActivity.class);
                startActivity(cropIntent);
                break;
        }
    }
}
