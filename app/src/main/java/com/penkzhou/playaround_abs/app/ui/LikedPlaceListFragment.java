package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.penkzhou.playaround_abs.app.OnProfileDataPass;
import com.penkzhou.playaround_abs.app.PlayAroundApplication;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.PlaceListAdapter;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14-2-26.
 */
public class LikedPlaceListFragment extends SherlockFragment implements AdapterView.OnItemClickListener {


    private PlaceListAdapter placeListAdapter;
    private ListView placeList;
    private TextView nullTip, loadingTextView;
    private LinearLayout loading;
    private AVUser currentUser;
    private OnProfileDataPass dataPasser;
    private String userId;


    public LikedPlaceListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_like_placelist, container, false);
        placeList = (ListView) rootView.findViewById(R.id.lv_like_placelist_list);
        nullTip = (TextView) rootView.findViewById(R.id.tv_like_placelist_nulltip);
        loadingTextView = (TextView) rootView.findViewById(R.id.tv_like_placelist_loading);
        loading = (LinearLayout) rootView.findViewById(R.id.layout_like_placelist_loading);
        setCurrentUser();
        return rootView;
    }

    public void setCurrentUser() {
        AVQuery<AVUser> query = AVUser.getQuery();
        query.whereEqualTo("objectId", userId);
        query.getFirstInBackground(new GetCallback<AVUser>() {
            @Override
            public void done(AVUser avUser, AVException e) {
                currentUser = avUser;
                loadLikedPlaces();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d("ProfileFragment", "onAttach");
        super.onAttach(activity);
        try {
            dataPasser = (OnProfileDataPass) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnProfileDataPass");
        }
        userId = dataPasser.getCurrentUserId();
        Log.d("终于得到了 userId", userId);
    }


    public void loadLikedPlaces() {
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.SOURCE, currentUser);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
        query.include(PlaceOperation.PLACE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            public void done(List<PlaceOperation> avObjects, AVException e) {
                if (e == null) {
                    ArrayList<Place> myPlaceList = new ArrayList<Place>();
                    for (PlaceOperation operation : avObjects) {
                        myPlaceList.add(operation.getPlace());
                    }
                    Log.d("成功", "查询到" + avObjects.size() + " 条符合条件的数据");
                    if (avObjects.size() > 0) {
                        placeListAdapter = new PlaceListAdapter(getActivity(), R.layout.place_list_item, myPlaceList, PlayAroundApplication.currentLocation);
                        placeList.setAdapter(placeListAdapter);
                        placeList.setOnItemClickListener(LikedPlaceListFragment.this);
                    } else {
                        placeList.setVisibility(View.GONE);
                        nullTip.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.d("失败", "查询错误: " + e.getMessage());
                }
                loading.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Place choosePlace = placeListAdapter.getItem(position);
        goToDetail(choosePlace);
    }

    public void goToDetail(Place place) {
        Intent intent = new Intent(getActivity(), PlaceInfoActivity.class);
        intent.putExtra("placeId", place.getObjectId());
        startActivity(intent);
    }


}
