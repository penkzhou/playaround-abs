package com.penkzhou.playaround_abs.app.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;

/**
 * Created by Administrator on 14-2-21.
 */
@AVClassName("PlaceComment")
public class PlaceComment extends AVObject {

    public final static String CONTENT = "content";
    public final static String SOURCE = "source";
    public final static String PLACE = "place";

    public PlaceComment() {
    }

    public String getContent() {
        return getString(CONTENT);
    }

    public void setContent(String content) {
        put(CONTENT, content);
    }

    public AVUser getSource() {
        return getAVUser(SOURCE);
    }

    public void setSource(AVUser source) {
        put(SOURCE, source);
    }

    public Place getPlace() {
        return (Place) get(PLACE);
    }

    public void setPlace(Place place) {
        put(PLACE, place);
    }
}
