package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.PopupClickListener;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.Place;

import java.util.List;

/**
 * Created by Administrator on 14-2-28.
 */
public class PlaceListInMapActivity extends SherlockFragmentActivity implements PopupClickListener, BDLocationListener {

    private final static int[] typeIcons = {R.drawable.basketball, R.drawable.table_tennis,
            R.drawable.tennis, R.drawable.football, R.drawable.badminton, R.drawable.snooker,
            R.drawable.volleyball, R.drawable.running, R.drawable.swimming, R.drawable.climbing, R.drawable.rockclimbing};
    private BMapManager mBMapMan = null;
    private MapView mMapView = null;
    private LayoutInflater mLayoutInflater;
    private LocationClient mLocationClient = null;
    private BDLocation currentLocation;
    private ProgressDialog pdl;
    private int distance;
    private List<Place> placeList;
    private MapController mMapController;
    private MyLocationOverlay myLocationOverlay;
    private LocationData locData;
    private MyItemizedOverlay itemOverlay;
    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBMapMan = new BMapManager(getApplication());
        mBMapMan.init("6icokEEqI3PPlhK7084LjA7x", null);
        setContentView(R.layout.activity_listmap);
        //注意：请在试用setContentView前初始化BMapManager对象，否则会报错
        init();
        new LoadingLocationTask().execute();
    }

    public void init() {
        mMapView = (MapView) findViewById(R.id.baiduMap);
        mMapView.setBuiltInZoomControls(true);
        mMapController = mMapView.getController();
        mLayoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        myLocationOverlay = new MyLocationOverlay(mMapView);
        locData = new LocationData();
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        pdl = new ProgressDialog(this);
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mMapView.destroy();
        if (mBMapMan != null) {
            mBMapMan.destroy();
            mBMapMan = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        if (mBMapMan != null) {
            mBMapMan.stop();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        if (mBMapMan != null) {
            mBMapMan.start();
        }
        super.onResume();
    }

    @Override
    public void onClickedPopup(int i) {
        Log.d("popup", "" + i);
    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        if (bdLocation != null) {
            currentLocation = bdLocation;
            bdLocation.setRadius(distance * 1000);
            mLocationClient.stop();
            GeoPoint point = new GeoPoint((int) (currentLocation.getLatitude() * 1E6), (int) (currentLocation.getLongitude() * 1E6));
            mMapController.setCenter(point);//设置地图中心点
            mMapController.setZoom(15);//设置地图zoom级别.
            //设置定位数据
            locData.latitude = bdLocation.getLatitude();
            locData.longitude = bdLocation.getLongitude();
            //如果不显示定位精度圈，将accuracy赋值为0即可
            locData.accuracy = bdLocation.getRadius();
            Log.d("accuracy", locData.accuracy + "");
            // 此处可以设置 locData的方向信息, 如果定位 SDK 未返回方向信息，用户可以自己实现罗盘功能添加方向信息。
            locData.direction = bdLocation.getDerect();
            myLocationOverlay.setData(locData);
            myLocationOverlay.setMarker(null);
            //添加定位图层
            myLocationOverlay.enableCompass();
            mMapView.getOverlays().add(myLocationOverlay);
            AVGeoPoint geoPoint = new AVGeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
            AVQuery<Place> query = new AVQuery<Place>("Place");
            query.whereWithinKilometers(Place.PLACELOCATION, geoPoint, distance);
            Log.d("distance", distance + "");
            query.findInBackground(new FindCallback<Place>() {
                public void done(List<Place> avObjects, AVException e) {
                    if (e == null) {
                        Log.d("成功", "查询到" + avObjects.size() + " 条符合条件的数据");
                        placeList = avObjects;
                        createPopup();
                        pdl.dismiss();
                    } else {
                        Log.d("失败", "查询错误: " + e.getMessage());
                    }
                }
            });
        }
    }

    @Override
    public void onReceivePoi(BDLocation bdLocation) {

    }

    /**
     * 根据返回的placeList生成对应的place popup
     */
    public void createPopup() {
        Drawable mark = getResources().getDrawable(R.drawable.icon_marka);
        itemOverlay = new MyItemizedOverlay(mark, mMapView);
        for (Place place : placeList) {
            GeoPoint point = new GeoPoint((int) (place.getPlaceLocation().getLatitude() * 1E6), (int) (place.getPlaceLocation().getLongitude() * 1E6));
            OverlayItem item = new OverlayItem(point, place.getPlaceName(), place.getPlaceAddress());
            itemOverlay.addItem(item);
        }
        mMapView.getOverlays().add(itemOverlay);
        mMapView.refresh();
    }

    class MyItemizedOverlay extends ItemizedOverlay<OverlayItem> {

        public MyItemizedOverlay(Drawable drawable, MapView mapView) {
            super(drawable, mapView);
        }

        protected boolean onTap(int index) {
            Log.d("onTap popup", "" + index);
            OverlayItem clickItem = itemOverlay.getItem(index);
            PopupOverlay popupOverlay = new PopupOverlay(mMapView, PlaceListInMapActivity.this);
            View myPlace = mLayoutInflater.inflate(R.layout.place_popup_item, null);
            TextView name = (TextView) myPlace.findViewById(R.id.tv_placepopup_name);
            TextView location = (TextView) myPlace.findViewById(R.id.tv_placepopup_distance);
            ImageButton gotoDetail = (ImageButton) myPlace.findViewById(R.id.ib_placepopup_detail);
            gotoDetail.setTag(index);
            gotoDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int chooseIndex = (Integer) v.getTag();
                    String choosePlaceId = placeList.get(chooseIndex).getObjectId();
                    Intent intent = new Intent(PlaceListInMapActivity.this, PlaceInfoActivity.class);
                    intent.putExtra("placeId", choosePlaceId);
                    startActivity(intent);
                }
            });
            name.setText(clickItem.getTitle());
            location.setText(clickItem.getSnippet());
            ImageView photo = (ImageView) myPlace.findViewById(R.id.iv_placepopup_logo);
            photo.setImageResource(typeIcons[placeList.get(index).getPlaceType().ordinal()]);
            GeoPoint point = new GeoPoint((int) (placeList.get(index).getPlaceLocation().getLatitude() * 1E6), (int) (placeList.get(index).getPlaceLocation().getLongitude() * 1E6));
            popupOverlay.showPopup(myPlace, point, 48);
            return true;
        }

        public boolean onTap(GeoPoint pt, MapView mapView) {
            //在此处理MapView的点击事件，当返回 true时
            super.onTap(pt, mapView);
            return false;
        }
    }

    private class LoadingLocationTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("LocSDK3", "doInBackground");
            if (mLocationClient != null && mLocationClient.isStarted())
                mLocationClient.requestLocation();
            else
                Log.d("LocSDK3", "locClient is null or not started");
            return null;
        }

        @Override
        protected void onPreExecute() {
            Log.d("LocSDK3", "onPreExecute");
            pdl.show();
            mLocationClient = new LocationClient(PlaceListInMapActivity.this);
            LocationClientOption option = new LocationClientOption();
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
            option.setCoorType("bd09ll");//返回的定位结果是百度经纬度，默认值gcj02
            option.setScanSpan(5000);
            option.setIsNeedAddress(true);//返回的定位结果包含地址信息
            option.setNeedDeviceDirect(true);//返回的定位结果包含手机机头的方向
            mLocationClient.setLocOption(option);
            mLocationClient.registerLocationListener(PlaceListInMapActivity.this);
            mLocationClient.start();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PlaceListInMapActivity.this);
            distance = Integer.parseInt(preferences.getString(getString(R.string.preference_distance_key), "50"));
            Log.d("LocSDK3", "onPreExecute--done!!");
        }
    }
}