package com.penkzhou.playaround_abs.app.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 14-3-8.
 */
public class PlaceList extends ArrayList<Place> implements Serializable {
    private String cacheKey;

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }
}
