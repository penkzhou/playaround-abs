package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.UserListAdapter;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14-3-1.
 */
public class LikeUserListActivity extends SherlockFragmentActivity implements AdapterView.OnItemClickListener {

    private UserListAdapter userListAdapter;
    private ListView userList;
    private ProgressDialog pdl;
    private ActionBar mActionBar;
    private String likePlaceId;
    private Place likePlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        likePlaceId = getIntent().getStringExtra("likePlaceId");
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setTitle(getString(R.string.like_userlist_title));
        setContentView(R.layout.activity_userlist);
        userList = (ListView) findViewById(R.id.lv_userlist_list);
        pdl = new ProgressDialog(this);
        pdl.setMessage(getString(R.string.loadstatus_bar_msg));
        likePlace = null;
        getLikePlace();
    }


    public void getLikePlace() {
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereEqualTo("objectId", likePlaceId);
        query.getFirstInBackground(new GetCallback<Place>() {
            @Override
            public void done(Place place, AVException e) {
                if (e == null) {
                    likePlace = place;
                    loadLikeUserList();
                } else {
                    Log.e("VisitUserListActivity--->getLikePlace Failed", e.getMessage());
                }
            }
        });
    }

    public void loadLikeUserList() {
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, likePlace);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
        query.include(PlaceOperation.SOURCE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            @Override
            public void done(List<PlaceOperation> placeOperations, AVException e) {
                if (e == null) {
                    ArrayList<AVUser> myUserList = new ArrayList<AVUser>();
                    for (PlaceOperation operation : placeOperations) {
                        myUserList.add(operation.getSource());
                    }
                    userListAdapter = new UserListAdapter(LikeUserListActivity.this, R.layout.user_list_item, myUserList);
                    userList.setAdapter(userListAdapter);
                    userList.setOnItemClickListener(LikeUserListActivity.this);
                    pdl.dismiss();
                } else {
                    Log.e("VisitUserListActivity--->loadVisitUserList", e.getMessage());
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AVUser user = userListAdapter.getItem(position);
        Intent intent = new Intent(LikeUserListActivity.this, ProfileActivity.class);
        intent.putExtra("objectId", user.getObjectId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }
}
