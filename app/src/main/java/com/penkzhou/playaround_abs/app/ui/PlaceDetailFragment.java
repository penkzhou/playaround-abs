package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround_abs.app.OnPlaceDataPass;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceOperation;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 14-2-26.
 */
public class PlaceDetailFragment extends SherlockFragment implements View.OnClickListener {


    private final static int[] typeIcons = {R.drawable.basketball, R.drawable.table_tennis,
            R.drawable.tennis, R.drawable.football, R.drawable.badminton, R.drawable.snooker,
            R.drawable.volleyball, R.drawable.running, R.drawable.swimming, R.drawable.climbing, R.drawable.rockclimbing};
    private String placeId;
    private Place currentPlace;
    private LayoutInflater mLayoutInflater;
    private LinearLayout visitUserListLayout, likeUserListLayout;
    private TextView placeName, placeDescribe, placeAddress, placeVisitTimes, placeLikeTimes, placeWarning, visitWarning, likeWarning;
    private boolean isVisited = false;
    private boolean isLiked = false;
    private ImageView placeLogo, placeTopview;
    private OnPlaceDataPass onPlaceDataPass;
    private View rootView;

    @Override
    public void onAttach(Activity activity) {
        Log.d("ProfileFragment", "onAttach");
        super.onAttach(activity);
        try {
            onPlaceDataPass = (OnPlaceDataPass) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnProfileDataPass");
        }
        placeId = onPlaceDataPass.getCurrentPlaceId();
    }

    public void loadPlace() {
        final ProgressDialog pdl = new ProgressDialog(getActivity());
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
        pdl.show();
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereEqualTo("objectId", placeId);
        query.limit(1);
        query.findInBackground(new FindCallback<Place>() {
            public void done(List<Place> avObjects, AVException e) {
                if (e == null) {
                    Log.d("成功", "loadPlace 查询到SinglePlace数据");
                    currentPlace = avObjects.get(0);
                    placeName.setText(currentPlace.getPlaceName());
                    placeDescribe.setText(currentPlace.getPlaceDescribe());
                    placeAddress.setText(currentPlace.getPlaceAddress());
                    placeLogo.setImageResource(typeIcons[currentPlace.getPlaceType().ordinal()]);
                    placeVisitTimes.setText(String.valueOf(currentPlace.getPlaceVisitTimes()));
                    placeLikeTimes.setText(String.valueOf(currentPlace.getPlaceLikeTimes()));
                    placeLikeTimes.setTag(true);
                    pdl.dismiss();
                    loadOperationLikeState();
                    loadOperationVisitState();
                    loadLikeList();
                    loadVisitList();
                } else {
                    Log.d("失败", "LoadPlace 查询错误: " + e.getMessage());
                }
            }
        });
    }

    public void updateLikeList() {
        likeWarning.setVisibility(View.GONE);
        LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
        ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
        AVUser user = AVUser.getCurrentUser();
        AVFile file = (AVFile) user.get("avatarUrl");
        Picasso.with(getActivity()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
        likeUserListLayout.addView(userItem, 0);
        likeUserListLayout.setVisibility(View.VISIBLE);
    }


    public void updateVisitList() {
        visitWarning.setVisibility(View.GONE);
        LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
        ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
        AVUser user = AVUser.getCurrentUser();
        AVFile file = (AVFile) user.get("avatarUrl");
        Picasso.with(getActivity()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
        visitUserListLayout.addView(userItem, 0);
        visitUserListLayout.setVisibility(View.VISIBLE);
    }

    public void loadOperationLikeState() {
        Log.d("loadOperationLikeState", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                if (e == null) {
                    Log.d("loadOperationLikeState 成功", "查询到" + i + " 条符合条件的数据");
                    if (i > 0) {
                        isLiked = true;
                        placeLikeTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_heart, 0, 0, 0);
                    } else {
                        isLiked = false;
                    }
                    placeLikeTimes.setTag(isLiked);
                } else {
                    Log.d("loadOperationLikeState 失败", e.getMessage());
                    placeLikeTimes.setTag(false);
                }
                Log.d("loadOperationLikeState", "------------- end----------");
            }
        });

    }

    public void loadOperationVisitState() {
        Log.d("loadOperationVisitState", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                if (e == null) {
                    Log.d("loadOperationVisitState成功", "查询到" + i + " 条符合条件的数据");
                    if (i > 0) {
                        isVisited = true;
                        placeVisitTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_location, 0, 0, 0);
                    } else {
                        isVisited = false;
                    }
                    placeVisitTimes.setTag(isVisited);
                } else {
                    Log.d("loadOperationVisitState失败", e.getMessage());
                    placeVisitTimes.setTag(false);
                }
                Log.d("loadOperationVisitState", "------------- end----------");
            }
        });
    }

    public void loadVisitList() {
        Log.d("loadVisitList", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
        query.include(PlaceOperation.SOURCE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            @Override
            public void done(List<PlaceOperation> placeOperations, AVException e) {
                if (e == null) {
                    if (placeOperations.size() == 0) {
                        visitWarning.setVisibility(View.VISIBLE);
                    } else {
                        for (PlaceOperation po : placeOperations) {
                            LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
                            final ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
                            AVUser user = po.getSource();
                            AVFile file = (AVFile) user.get("avatarUrl");
                            Picasso.with(getActivity()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
                            if (user != AVUser.getCurrentUser()) {
                                visitUserListLayout.addView(userItem);
                            } else {
                                visitUserListLayout.addView(userItem, 0);
                            }
                        }
                        visitUserListLayout.setVisibility(View.VISIBLE);

                    }
                } else {
                    Log.e("失败", "loadVisitList 查询错误: " + e.getMessage());
                    visitUserListLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    public void loadLikeList() {
        Log.d("loadVisitList", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
        query.include(PlaceOperation.SOURCE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            @Override
            public void done(List<PlaceOperation> placeOperations, AVException e) {
                if (e == null) {
                    if (placeOperations.size() == 0) {
                        likeWarning.setVisibility(View.VISIBLE);
                    } else {

                        for (PlaceOperation po : placeOperations) {
                            LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
                            ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
                            AVUser user = po.getSource();
                            AVFile file = (AVFile) user.get("avatarUrl");
                            Picasso.with(getActivity()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
                            if (user != AVUser.getCurrentUser()) {
                                likeUserListLayout.addView(userItem);
                            } else {
                                likeUserListLayout.addView(userItem, 0);
                            }
                        }
                        likeUserListLayout.setVisibility(View.VISIBLE);

                    }
                } else {
                    Log.e("失败", "loadVisitList 查询错误: " + e.getMessage());
                    likeUserListLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_place_detail, container, false);
        init();
        loadPlace();
        return rootView;
    }


    public void init() {
        mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        placeName = (TextView) rootView.findViewById(R.id.tv_placedetail_name);
        placeDescribe = (TextView) rootView.findViewById(R.id.tv_placedetail_describe);
        placeAddress = (TextView) rootView.findViewById(R.id.tv_placedetail_location);
        placeWarning = (TextView) rootView.findViewById(R.id.tv_placedetail_warning);
        placeLogo = (ImageView) rootView.findViewById(R.id.iv_placedetail_logo);
        placeTopview = (ImageView) rootView.findViewById(R.id.iv_placedetail_topview);
        placeVisitTimes = (TextView) rootView.findViewById(R.id.tv_placedetail_visit);
        placeLikeTimes = (TextView) rootView.findViewById(R.id.tv_placedetail_heart);
        visitUserListLayout = (LinearLayout) rootView.findViewById(R.id.linear_placedetail_visitlist);
        likeUserListLayout = (LinearLayout) rootView.findViewById(R.id.linear_placedetail_likelist);
        visitWarning = (TextView) rootView.findViewById(R.id.warning_placedetail_visitlist);
        likeWarning = (TextView) rootView.findViewById(R.id.warning_placedetail_likelist);
        placeLikeTimes.setOnClickListener(this);
        placeVisitTimes.setOnClickListener(this);
        likeUserListLayout.setOnClickListener(this);
        visitUserListLayout.setOnClickListener(this);
    }


    public void makeVisitOperation() {
        Log.d("makeVisitOperation", "------------- start----------");
        int currentVisitTimes = 0;
        if (!(Boolean) placeVisitTimes.getTag()) {
            placeVisitTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_location, 0, 0, 0);
            currentVisitTimes = Integer.valueOf(placeVisitTimes.getText().toString());
            placeVisitTimes.setText(String.valueOf(currentVisitTimes + 1));
            PlaceOperation po = new PlaceOperation();
            po.setSource(AVUser.getCurrentUser());
            po.setPlace(currentPlace);
            po.setOperationType(PlaceOperation.VISIT);
            currentPlace.increment(Place.PLACEVISITTIMES);
            po.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        isVisited = true;
                        placeVisitTimes.setTag(isVisited);
                        currentPlace.saveInBackground();
                        updateVisitList();
                        Log.d("PlaceOperation.VISTT", " 操作成功");
                        Toast.makeText(getActivity(), "签到成功", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("PlaceOperation.VISTT", " 操作失败");
                    }
                    Log.d("makeVisitOperation", "------------- end----------");
                }
            });
        } else {
            Log.d("makeVisitOperation", "------------- else ----------");
            placeVisitTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_location_off, 0, 0, 0);
            currentVisitTimes = Integer.valueOf(placeVisitTimes.getText().toString());
            placeVisitTimes.setText(String.valueOf(currentVisitTimes - 1));
            AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
            query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
            query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
            query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
            currentPlace.increment(Place.PLACEVISITTIMES, -1);
            visitUserListLayout.removeViewAt(0);
            currentPlace.saveInBackground();
            isVisited = false;
            placeVisitTimes.setTag(isVisited);
            query.getFirstInBackground(new GetCallback<PlaceOperation>() {
                @Override
                public void done(PlaceOperation placeOperation, AVException e) {
                    if (e == null) {
                        placeOperation.deleteInBackground();
                        Toast.makeText(getActivity(), "您已取消签到", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("makeVisitOperation error", "------------- end ----------");
                    }
                    Log.d("makeVisitOperation", "------------- end ----------");
                }
            });
        }
    }

    public void makeLikeOperation() {
        Log.d("makeLikeOperation", "------------- start----------");
        int currentLikeTimes = 0;
        if (!(Boolean) placeLikeTimes.getTag()) {
            placeLikeTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_heart, 0, 0, 0);
            currentLikeTimes = Integer.valueOf(placeLikeTimes.getText().toString());
            placeLikeTimes.setText(String.valueOf(currentLikeTimes + 1));
            PlaceOperation po = new PlaceOperation();
            po.setSource(AVUser.getCurrentUser());
            po.setPlace(currentPlace);
            po.setOperationType(PlaceOperation.LIKE);
            currentPlace.increment(Place.PLACELIKETIMES);
            po.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        isLiked = true;
                        placeLikeTimes.setTag(isLiked);
                        currentPlace.saveInBackground();
                        updateLikeList();
                        Log.d("PlaceOperation.LIKE", " 操作成功");
                        Toast.makeText(getActivity(), "收藏成功", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("PlaceOperation.LIKE", " 操作失败" + e.getCode());
                    }
                    Log.d("makeLikeOperation", "------------- end----------");
                }
            });
        } else {
            Log.d("makeLikeOperation", "------------- else ----------");
            placeLikeTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_heart_off, 0, 0, 0);
            currentLikeTimes = Integer.valueOf(placeLikeTimes.getText().toString());
            placeLikeTimes.setText(String.valueOf(currentLikeTimes - 1));
            AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
            query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
            query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
            query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
            currentPlace.increment(Place.PLACELIKETIMES, -1);
            currentPlace.saveInBackground();
            likeUserListLayout.removeViewAt(0);
            isLiked = false;
            placeLikeTimes.setTag(isLiked);
            query.getFirstInBackground(new GetCallback<PlaceOperation>() {
                @Override
                public void done(PlaceOperation placeOperation, AVException e) {
                    if (e == null) {
                        placeOperation.deleteInBackground();
                        Toast.makeText(getActivity(), "您已取消收藏", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("makeLikeOperation error", "取消收藏失败 " + e.getCode());
                    }
                    Log.d("makeLikeOperation", "------------- end ----------");
                }
            });
            query.findInBackground(new FindCallback<PlaceOperation>() {
                @Override
                public void done(List<PlaceOperation> placeOperations, AVException e) {
                    if (e == null) {
                        if (placeOperations.size() > 0) {
                            placeOperations.get(0).deleteInBackground();
                            Toast.makeText(getActivity(), "您已取消收藏", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d("makeLikeOperation error", "取消收藏失败 " + e.getCode());
                    }
                    Log.d("makeLikeOperation", "------------- end ----------");
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        currentPlace.setFetchWhenSave(true);
        switch (v.getId()) {
            case R.id.tv_placedetail_heart:
                makeLikeOperation();
                break;
            case R.id.tv_placedetail_visit:
                makeVisitOperation();
                break;
            case R.id.linear_placedetail_likelist:
                Intent toLike = new Intent(getActivity(), LikeUserListActivity.class);
                toLike.putExtra("likePlaceId", currentPlace.getObjectId());
                startActivity(toLike);
                break;
            case R.id.linear_placedetail_visitlist:
                Intent toVisit = new Intent(getActivity(), VisitUserListActivity.class);
                toVisit.putExtra("visitPlaceId", currentPlace.getObjectId());
                startActivity(toVisit);
                break;
        }
    }
}
