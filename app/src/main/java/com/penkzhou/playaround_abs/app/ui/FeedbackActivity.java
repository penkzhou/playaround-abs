package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.Feedback;

public class FeedbackActivity extends SherlockFragmentActivity implements View.OnClickListener {
    private Button sendButton;
    private EditText feedbackContent;
    private String feedbackString;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setTitle(getString(R.string.feedback_text_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void init() {
        sendButton = (Button) findViewById(R.id.btn_feedback_send);
        feedbackContent = (EditText) findViewById(R.id.et_feedback_content);
        sendButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_feedback_send:
                if (TextUtils.isEmpty(feedbackContent.getText())) {
                    Toast.makeText(this, getString(R.string.feedback_text_nulltip), Toast.LENGTH_LONG).show();
                    return;
                }
                feedbackString = feedbackContent.getText().toString();
                Feedback feedback = new Feedback();
                feedback.setSource(AVUser.getCurrentUser());
                feedback.setContent(feedbackString);
                feedback.setDevice(Build.DEVICE);
                feedback.setProduct(Build.PRODUCT);
                feedback.setModel(Build.MODEL);
                feedback.setSDK(Build.VERSION.RELEASE);
                feedback.setOsVersion(System.getProperty("os.version"));
                final ProgressDialog pdl = new ProgressDialog(FeedbackActivity.this);
                pdl.setTitle(getString(R.string.feedback_text_title));
                pdl.setMessage(getString(R.string.feedback_text_tip));
                pdl.show();
                feedback.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        pdl.dismiss();
                        if (e == null) {
                            feedbackContent.setText("");
                            Toast.makeText(FeedbackActivity.this, getString(R.string.feedback_text_thanks), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(FeedbackActivity.this, getString(R.string.feedback_text_failed), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                break;
        }
    }

}
