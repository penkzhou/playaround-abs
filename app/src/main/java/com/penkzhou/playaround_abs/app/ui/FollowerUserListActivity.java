package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.UserListAdapter;

import java.util.List;

/**
 * Created by Administrator on 14-3-1.
 */
public class FollowerUserListActivity extends SherlockFragmentActivity implements AdapterView.OnItemClickListener {

    private UserListAdapter userListAdapter;
    private ListView userList;
    private ProgressDialog pdl;
    private ActionBar mActionBar;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userId = getIntent().getStringExtra("userId");
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setTitle("关注他的人");

        setContentView(R.layout.activity_userlist);
        userList = (ListView) findViewById(R.id.lv_userlist_list);
        pdl = new ProgressDialog(this);
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
        loadFollowerUserList();
    }



    public void loadFollowerUserList() {
        pdl.show();
        AVUser.followerQuery(userId, AVUser.class).include("follower").findInBackground(new FindCallback<AVUser>() {
            @Override
            public void done(List<AVUser> avUsers, AVException e) {
                pdl.dismiss();
                if (e == null) {
                    userListAdapter = new UserListAdapter(FollowerUserListActivity.this, R.layout.user_list_item, avUsers);
                    userList.setAdapter(userListAdapter);
                    userList.setOnItemClickListener(FollowerUserListActivity.this);
                } else {
                    //e.printStackTrace();
                    Log.e("loadFollowerUserList", e.getMessage());
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AVUser user = userListAdapter.getItem(position);
        Intent intent = new Intent(FollowerUserListActivity.this, ProfileActivity.class);
        intent.putExtra("objectId", user.getObjectId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }
}
