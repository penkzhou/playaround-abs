package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVStatus;
import com.avos.avoscloud.AVStatusQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.penkzhou.playaround_abs.app.MainActivity;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.AVStatusAdapter;

import java.util.List;

/**
 * Created by Administrator on 14-2-19.
 */
public class StatusListFragment extends SherlockFragment {
    MainActivity mParent;
    private AVStatusAdapter avStatusAdapter;
    private ProgressDialog pdl;
    private ListView mDrawerListView;

    public StatusListFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;
    }

    //@TODO fix the timeline bug.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_statuslist, container, false);
        mDrawerListView = (ListView) rootView.findViewById(R.id.lv_statuslist);
        pdl = new ProgressDialog(getActivity());
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
        pdl.show();
        AVStatusQuery inboxQuery = AVStatus.inboxQuery(AVUser.getCurrentUser(), AVStatus.INBOX_TYPE.TIMELINE.toString());
        inboxQuery.setLimit(50);  //设置最多返回50条状态
        inboxQuery.setSinceId(0);  //查询返回的status的messageId必须大于sinceId，默认为0
        inboxQuery.findInBackground(new FindCallback<AVStatus>() {
            @Override
            public void done(List<AVStatus> avStatuses, AVException e) {
                if (e == null) {
                    avStatusAdapter = new AVStatusAdapter(getActivity(), R.id.lv_statuslist, avStatuses);
                    mDrawerListView.setAdapter(avStatusAdapter);
                } else {
                    Toast.makeText(getActivity(), getText(R.string.statuslist_error_load), Toast.LENGTH_LONG).show();
                    Log.e("Status list获取失败", " list获取失败 " + e.getMessage());
                    e.printStackTrace();
                }
                pdl.dismiss();
            }
        });
        return rootView;
    }
}
