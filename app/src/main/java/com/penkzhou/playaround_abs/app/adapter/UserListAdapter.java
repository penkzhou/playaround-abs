package com.penkzhou.playaround_abs.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVUser;
import com.penkzhou.playaround_abs.app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 14-1-17.
 */
public class UserListAdapter extends ArrayAdapter<AVUser> {
    private Context activity;
    private List<AVUser> objects;

    public UserListAdapter(Context context, int resource, List<AVUser> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.user_list_item, null);
            viewHolder = new ViewHolder();
            assert v != null;
            viewHolder.userName = (TextView) v.findViewById(R.id.tv_user_name);
            viewHolder.userEmail = (TextView) v.findViewById(R.id.tv_user_email);
            viewHolder.userDescribe = (TextView) v.findViewById(R.id.tv_user_desc);
            viewHolder.logo = (ImageView) v.findViewById(R.id.iv_user_logo);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final AVUser user = objects.get(position);

        if (user != null) {
            viewHolder.userName.setText(user.getUsername());
            viewHolder.userEmail.setText(user.getEmail());
            viewHolder.userDescribe.setText(user.getString("intro"));
            AVFile file = (AVFile) user.get("avatarUrl");
            Picasso.with(getContext()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(viewHolder.logo);
        }
        return v;
    }

    public static class ViewHolder {
        public TextView userName, userEmail, userDescribe;
        public ImageView logo;

    }
}
