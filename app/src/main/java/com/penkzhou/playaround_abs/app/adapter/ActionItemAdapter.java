package com.penkzhou.playaround_abs.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.ActionItem;

import java.util.ArrayList;

/**
 * Created by Administrator on 14-2-21.
 */
public class ActionItemAdapter extends ArrayAdapter<ActionItem> {
    private ArrayList<ActionItem> iconList;
    private Activity context;

    public ActionItemAdapter(Activity context, int resource, ArrayList<ActionItem> iconList) {
        super(context, resource, iconList);
        this.iconList = iconList;
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.place_type_item, parent, false);
        }
        ActionItem current = iconList.get(position);
        ImageView type_icon = (ImageView) row.findViewById(R.id.iv_typeitem_icon);
        type_icon.setImageResource(current.getActionIconId());
        TextView type_name = (TextView) row.findViewById(R.id.tv_typeitem_desc);
        type_name.setText(current.getActionName());
        return row;
    }

}
