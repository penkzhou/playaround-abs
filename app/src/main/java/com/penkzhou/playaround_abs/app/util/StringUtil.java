package com.penkzhou.playaround_abs.app.util;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 06peng
 */
public class StringUtil {


    public static String cutString(String value, int start, int length) {
        if (value == null || value.length() <= 0) {
            value = "……";
        } else if (value.length() <= length) {
            value = value;
        } else {
            value = value.substring(0, length) + "……";
        }
        return value;
    }

    /**
     * 把字符串数组用逗号隔开
     *
     * @param arrays
     * @return
     */

    public static String ArraysToString(String[] arrays) {
        String result = "";
        if (arrays != null && arrays.length > 0) {
            for (String value : arrays) {
                result += value + ",";
            }
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public static String intArraysToString(int[] arrays) {
        String result = "";
        if (arrays != null && arrays.length > 0) {
            for (int value : arrays) {
                result += value + " ";
            }
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public static boolean isBlank(String str) {
        return str == null || str.equals("");
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static SpannableStringBuilder hightLinghtString(String destStr, String subStr, int color) {
        SpannableStringBuilder style = new SpannableStringBuilder(null == destStr ? "" : destStr);
        if (null == destStr || "".equals(destStr) || "".equals(subStr)) {
            return style;
        }
        int start = destStr.lastIndexOf(subStr);
        if (start != -1) {
            style.setSpan(new ForegroundColorSpan(color), start, start + subStr.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        return style;
    }

    /**
     * 验证邮箱
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        if (StringUtil.isBlank(email)) {
            return false;
        }
        return email.matches("^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$");
    }

    /**
     * 检测英文，数字，下划线
     *
     * @param account
     * @return
     */
    public static boolean checkLoginAccount(String account) {
        boolean valid = true;
        if (StringUtil.isBlank(account)) {
            return false;
        }
        for (int i = 0; i < account.length(); i++) {
            char ch = account.charAt(i);
            if (Character.isLetterOrDigit(ch) || ch == '_') {
                continue;
            } else {
                valid = false;
                break;
            }
        }
        return valid;
    }

    /**
     * 判断是否手机号
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobile(String mobiles) {
        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
        Matcher m = p.matcher(mobiles);
        return m.matches();
    }

}