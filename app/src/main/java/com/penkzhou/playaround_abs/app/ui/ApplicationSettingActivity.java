package com.penkzhou.playaround_abs.app.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround_abs.app.R;


/**
 * Created by Administrator on 14-2-28.
 */
public class ApplicationSettingActivity extends SherlockPreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SharedPreferences preference;
    private AVUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
        preference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        currentUser = AVUser.getCurrentUser();
        initSummary();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    public void initSummary() {
        EditTextPreference usernamePref = (EditTextPreference) findPreference("username");
        usernamePref.setSummary(preference.getString("username", "编辑用户名"));
        EditTextPreference emaillTextPref = (EditTextPreference) findPreference("email");
        emaillTextPref.setSummary(preference.getString("email", "编辑Email"));
        EditTextPreference introTextPref = (EditTextPreference) findPreference("intro");
        introTextPref.setSummary(preference.getString("intro", "编辑个人简介"));
    }


    @Override
    protected void onResume() {
        super.onResume();
        preference.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        preference.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("email")) {
            String newEmail = sharedPreferences.getString(key, "");
            sharedPreferences.edit().putString(key, newEmail).commit();
            currentUser.setEmail(newEmail);
            currentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        Toast.makeText(getApplicationContext(), "更新Email成功。", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("update_email", e.getMessage());
                        Toast.makeText(getApplicationContext(), "更新Email失败。", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        if (key.equals("intro")) {
            String newValue = sharedPreferences.getString(key, "");
            sharedPreferences.edit().putString(key, newValue).commit();
            currentUser.put(key, newValue);
            currentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        Toast.makeText(getApplicationContext(), "更新个人简介成功。", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("update_email", e.getMessage());
                        Toast.makeText(getApplicationContext(), "更新个人简介失败。", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
