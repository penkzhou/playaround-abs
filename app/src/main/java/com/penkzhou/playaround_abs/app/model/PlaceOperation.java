package com.penkzhou.playaround_abs.app.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;

/**
 * Created by Administrator on 14-2-21.
 */
@AVClassName("PlaceOperation")
public class PlaceOperation extends AVObject {

    public final static String OPERATION_TYPE = "operationType";
    public final static String SOURCE = "source";
    public final static String PLACE = "place";

    public final static int LIKE = 0;
    public final static int VISIT = 1;

    public PlaceOperation() {
    }

    public int getOperationType() {
        return getInt(OPERATION_TYPE);
    }

    public void setOperationType(int type) {
        put(OPERATION_TYPE, type);
    }

    public AVUser getSource() {
        return getAVUser(SOURCE);
    }

    public void setSource(AVUser source) {
        put(SOURCE, source);
    }

    public Place getPlace() {
        return (Place) get(PLACE);
    }

    public void setPlace(Place place) {
        put(PLACE, place);
    }
}
