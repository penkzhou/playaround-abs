package com.penkzhou.playaround_abs.app.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.penkzhou.playaround_abs.app.PlayAroundApplication;
import com.penkzhou.playaround_abs.app.R;

/**
 * Created by Administrator on 14-3-14.
 */
public class AboutActivity extends SherlockFragmentActivity {
    private TextView appname, version, author, source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        appname = (TextView) findViewById(R.id.tv_about_appname);
        appname.setTypeface(PlayAroundApplication.boldTF);
        version = (TextView) findViewById(R.id.tv_about_version);
        version.setTypeface(PlayAroundApplication.lightTF);
        author = (TextView) findViewById(R.id.tv_about_author);
        author.setTypeface(PlayAroundApplication.lightTF);
        source = (TextView) findViewById(R.id.tv_about_source);
        source.setTypeface(PlayAroundApplication.regularTF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return false;
    }
}
