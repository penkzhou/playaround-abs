package com.penkzhou.playaround_abs.app.model;

/**
 * Created by Administrator on 14-2-21.
 */
public class ActionItem {
    private String actionName;
    private int actionIconId;

    public ActionItem(String actionName, int actionIconId) {
        this.actionName = actionName;
        this.actionIconId = actionIconId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public int getActionIconId() {
        return actionIconId;
    }

    public void setActionIconId(int actionIconId) {
        this.actionIconId = actionIconId;
    }
}
