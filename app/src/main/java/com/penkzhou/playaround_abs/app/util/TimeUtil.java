package com.penkzhou.playaround_abs.app.util;

/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;

import com.penkzhou.playaround_abs.app.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TimeUtil {


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final DateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    public static String getTimeAgo(Date datetime, Context ctx) {
        long time = datetime.getTime();
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }
        Calendar c = Calendar.getInstance();
        long now = c.getTimeInMillis();


        if (time > now || time <= 0) {
            return null;
        }

// TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return TextUtil.getStringFromResource(R.string.just_now, ctx);
        } else if (diff < 2 * MINUTE_MILLIS) {
            return TextUtil.getStringFromResource(R.string.a_minute_ago, ctx);
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + TextUtil.getStringFromResource(R.string.minutes_ago, ctx);
        } else if (diff < 90 * MINUTE_MILLIS) {
            return TextUtil.getStringFromResource(R.string.an_hour_ago, ctx);
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + TextUtil.getStringFromResource(R.string.hours_ago, ctx);
        } else if (diff < 48 * HOUR_MILLIS) {
            return TextUtil.getStringFromResource(R.string.a_day_ago, ctx);
        } else {
            return df.format(new Date(time));
            //return diff / DAY_MILLIS + TextUtil.getStringFromResource(R.string.days_ago, ctx);
        }
    }
}