package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.GetCallback;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MKMapViewListener;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.PopupClickListener;
import com.baidu.mapapi.map.RouteOverlay;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPlanNode;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKShareUrlResult;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.penkzhou.playaround_abs.app.OnPlaceDataPass;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.Place;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14-2-28.
 */
public class PlaceNavigationFragment extends SherlockFragment implements PopupClickListener, BDLocationListener, MKSearchListener, View.OnClickListener, MKMapViewListener {

    private BMapManager mBMapMan = null;
    private MapView mMapView = null;
    private LocationClient mLocationClient = null;
    private BDLocation currentLocation;
    private ProgressDialog pdl, naviPdl;
    private String placeId;
    private Place currentPlace;
    private MapController mMapController;
    private MyLocationOverlay myLocationOverlay;
    private LocationData locData;
    private MyItemizedOverlay itemOverlay;
    private OnPlaceDataPass onPlaceDataPass;
    private Button naviButton;
    private Spinner naviStyle;
    private View rootView;
    private MKSearch mMKSearch;
    private List<RouteOverlay> allRouteOverlay;
    private RouteOverlay driveOverlay, busOverlay, walkOverlay;

    @Override
    public void onAttach(Activity activity) {
        Log.d("ProfileFragment", "onAttach");
        super.onAttach(activity);
        try {
            onPlaceDataPass = (OnPlaceDataPass) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnProfileDataPass");
        }
        placeId = onPlaceDataPass.getCurrentPlaceId();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBMapMan = new BMapManager(getSherlockActivity().getApplication());
        mBMapMan.init("6icokEEqI3PPlhK7084LjA7x", null);
        rootView = inflater.inflate(R.layout.fragment_navto_place, container, false);
        init();
        new LoadingLocationTask().execute();
        return rootView;
    }

    public void init() {
        mMapView = (MapView) rootView.findViewById(R.id.mv_place_navi);
        naviButton = (Button) rootView.findViewById(R.id.btn_place_navi_go);
        naviStyle = (Spinner) rootView.findViewById(R.id.sp_place_navi_style);
        naviButton.setOnClickListener(this);
        mMapView.setBuiltInZoomControls(true);

        mMapController = mMapView.getController();
        myLocationOverlay = new MyLocationOverlay(mMapView);
        locData = new LocationData();
        mMKSearch = new MKSearch();
        mMKSearch.init(mBMapMan, this);
        mMapView.regMapViewListener(mBMapMan, this);
        allRouteOverlay = new ArrayList<RouteOverlay>();
        pdl = new ProgressDialog(getSherlockActivity());
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
        naviPdl = new ProgressDialog(getSherlockActivity());
        naviPdl.setMessage(getText(R.string.navigating_bar_msg));
    }


    @Override
    public void onDestroy() {
        mMapView.destroy();
        if (mBMapMan != null) {
            mBMapMan.destroy();
            mBMapMan = null;
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        if (mBMapMan != null) {
            mBMapMan.stop();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        if (mBMapMan != null) {
            mBMapMan.start();
        }
        super.onResume();
    }

    @Override
    public void onClickedPopup(int i) {
        Log.d("popup", "" + i);
    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        if (bdLocation != null) {
            currentLocation = bdLocation;
            bdLocation.setRadius(20 * 1000);
            mLocationClient.stop();
            GeoPoint point = new GeoPoint((int) (currentLocation.getLatitude() * 1E6), (int) (currentLocation.getLongitude() * 1E6));
            mMapController.setCenter(point);//设置地图中心点
            mMapController.setZoom(15);//设置地图zoom级别.
            //设置定位数据
            locData.latitude = bdLocation.getLatitude();
            locData.longitude = bdLocation.getLongitude();
            //如果不显示定位精度圈，将accuracy赋值为0即可
            locData.accuracy = bdLocation.getRadius();
            // 此处可以设置 locData的方向信息, 如果定位 SDK 未返回方向信息，用户可以自己实现罗盘功能添加方向信息。
            locData.direction = bdLocation.getDerect();
            myLocationOverlay.setData(locData);
            myLocationOverlay.setMarker(null);
            //添加定位图层
            myLocationOverlay.enableCompass();
            mMapView.getOverlays().add(myLocationOverlay);
            AVGeoPoint geoPoint = new AVGeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
            AVQuery<Place> query = new AVQuery<Place>("Place");
            query.getInBackground(placeId, new GetCallback<Place>() {
                @Override
                public void done(Place place, AVException e) {
                    if (e == null) {
                        currentPlace = place;
                        createPopup();
//                        naviSetup();
                    } else {
                        Log.d("get place wrong", "error code : " + e.getCode());
                    }
                    pdl.dismiss();
                }
            });
        }
    }

    public void driveNaviSetup() {
        MKPlanNode start = new MKPlanNode();
        start.pt = new GeoPoint((int) (currentLocation.getLatitude() * 1E6), (int) (currentLocation.getLongitude() * 1E6));
        MKPlanNode end = new MKPlanNode();
        end.pt = new GeoPoint((int) (currentPlace.getPlaceLocation().getLatitude() * 1E6), (int) (currentPlace.getPlaceLocation().getLongitude() * 1E6));// 设置驾车路线搜索策略，时间优先、费用最少或距离最短
        mMKSearch.setDrivingPolicy(MKSearch.ECAR_TIME_FIRST);
        mMKSearch.drivingSearch(null, start, null, end);
    }


    public void walkNaviSetup() {
        MKPlanNode start = new MKPlanNode();
        start.pt = new GeoPoint((int) (currentLocation.getLatitude() * 1E6), (int) (currentLocation.getLongitude() * 1E6));
        MKPlanNode end = new MKPlanNode();
        end.pt = new GeoPoint((int) (currentPlace.getPlaceLocation().getLatitude() * 1E6), (int) (currentPlace.getPlaceLocation().getLongitude() * 1E6));// 设置驾车路线搜索策略，时间优先、费用最少或距离最短
        mMKSearch.setDrivingPolicy(MKSearch.ECAR_TIME_FIRST);
        mMKSearch.walkingSearch(null, start, null, end);
    }


    public void busNaviSetup() {
        MKPlanNode start = new MKPlanNode();
        start.pt = new GeoPoint((int) (currentLocation.getLatitude() * 1E6), (int) (currentLocation.getLongitude() * 1E6));
        MKPlanNode end = new MKPlanNode();
        end.pt = new GeoPoint((int) (currentPlace.getPlaceLocation().getLatitude() * 1E6), (int) (currentPlace.getPlaceLocation().getLongitude() * 1E6));// 设置驾车路线搜索策略，时间优先、费用最少或距离最短
        mMKSearch.setTransitPolicy(MKSearch.ECAR_TIME_FIRST);
        mMKSearch.transitSearch(currentLocation.getCity(), start, end);
    }

    @Override
    public void onReceivePoi(BDLocation bdLocation) {

    }

    /**
     * 根据返回的placeList生成对应的place popup
     */
    public void createPopup() {
        Drawable mark = getResources().getDrawable(R.drawable.icon_marka);
        itemOverlay = new MyItemizedOverlay(mark, mMapView);
        GeoPoint point = new GeoPoint((int) (currentPlace.getPlaceLocation().getLatitude() * 1E6), (int) (currentPlace.getPlaceLocation().getLongitude() * 1E6));
        OverlayItem item = new OverlayItem(point, currentPlace.getPlaceName(), currentPlace.getPlaceAddress());
        itemOverlay.addItem(item);
        mMapView.getOverlays().add(itemOverlay);
        mMapView.refresh();
    }

    @Override
    public void onGetPoiResult(MKPoiResult mkPoiResult, int i, int i2) {

    }

    @Override
    public void onGetTransitRouteResult(MKTransitRouteResult mkTransitRouteResult, int i) {
        if (mkTransitRouteResult == null) {
            return;
        }
        RouteOverlay routeOverlay = new RouteOverlay(getSherlockActivity(), mMapView);  // 此处仅展示一个方案作为示例
        routeOverlay.setData(mkTransitRouteResult.getPlan(0).getRoute(0));
        mMapView.getOverlays().removeAll(allRouteOverlay);
        mMapView.getOverlays().add(routeOverlay);
        allRouteOverlay.add(routeOverlay);
        mMapView.refresh();
    }

    @Override
    public void onGetDrivingRouteResult(MKDrivingRouteResult mkDrivingRouteResult, int i) {
        naviPdl.dismiss();
        if (mkDrivingRouteResult == null) {
            return;
        }
        RouteOverlay routeOverlay = new RouteOverlay(getSherlockActivity(), mMapView);  // 此处仅展示一个方案作为示例
        routeOverlay.setData(mkDrivingRouteResult.getPlan(0).getRoute(0));
        mMapView.getOverlays().removeAll(allRouteOverlay);
        mMapView.getOverlays().add(routeOverlay);
        allRouteOverlay.add(routeOverlay);
        mMapView.refresh();
        mMapController.animateTo(mkDrivingRouteResult.getEnd().pt);//设置地图中心点
    }

    @Override
    public void onGetWalkingRouteResult(MKWalkingRouteResult mkWalkingRouteResult, int i) {
        naviPdl.dismiss();
        if (mkWalkingRouteResult == null) {
            return;
        }
        RouteOverlay routeOverlay = new RouteOverlay(getSherlockActivity(), mMapView);  // 此处仅展示一个方案作为示例
        routeOverlay.setData(mkWalkingRouteResult.getPlan(0).getRoute(0));
        mMapView.getOverlays().removeAll(allRouteOverlay);
        mMapView.getOverlays().add(routeOverlay);
        allRouteOverlay.add(routeOverlay);
        mMapView.refresh();
        mMapController.animateTo(mkWalkingRouteResult.getEnd().pt);//设置地图中心点
    }

    @Override
    public void onGetAddrResult(MKAddrInfo mkAddrInfo, int i) {

    }

    @Override
    public void onGetBusDetailResult(MKBusLineResult mkBusLineResult, int i) {

    }

    @Override
    public void onGetSuggestionResult(MKSuggestionResult mkSuggestionResult, int i) {

    }

    @Override
    public void onGetPoiDetailSearchResult(int i, int i2) {

    }

    @Override
    public void onGetShareUrlResult(MKShareUrlResult mkShareUrlResult, int i, int i2) {

    }

    @Override
    public void onClick(View v) {
        naviPdl.show();
        switch (naviStyle.getSelectedItemPosition()) {
            case 0:
                driveNaviSetup();
                break;
            case 1:
                walkNaviSetup();
                break;
//            case 2:
//                busNaviSetup();
//                break;
        }
    }

    @Override
    public void onMapMoveFinish() {

    }

    @Override
    public void onClickMapPoi(MapPoi mapPoi) {

    }

    @Override
    public void onGetCurrentMap(Bitmap bitmap) {

    }

    @Override
    public void onMapAnimationFinish() {

    }

    @Override
    public void onMapLoadFinish() {
        naviButton.setEnabled(true);
    }

    class MyItemizedOverlay extends ItemizedOverlay<OverlayItem> {

        public MyItemizedOverlay(Drawable drawable, MapView mapView) {
            super(drawable, mapView);
        }

        protected boolean onTap(int index) {
            Log.d("onTap popup", "" + index);
            return true;
        }

        public boolean onTap(GeoPoint pt, MapView mapView) {
            //在此处理MapView的点击事件，当返回 true时
            super.onTap(pt, mapView);
            return false;
        }
    }

    private class LoadingLocationTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("LocSDK3", "doInBackground");
            if (mLocationClient != null && mLocationClient.isStarted())
                mLocationClient.requestLocation();
            else
                Log.d("LocSDK3", "locClient is null or not started");
            return null;
        }

        @Override
        protected void onPreExecute() {
            Log.d("LocSDK3", "onPreExecute");
            pdl.show();
            mLocationClient = new LocationClient(getSherlockActivity());
            LocationClientOption option = new LocationClientOption();
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
            option.setCoorType("bd09ll");//返回的定位结果是百度经纬度，默认值gcj02
            option.setScanSpan(5000);
            option.setIsNeedAddress(true);//返回的定位结果包含地址信息
            option.setNeedDeviceDirect(true);//返回的定位结果包含手机机头的方向
            mLocationClient.setLocOption(option);
            mLocationClient.registerLocationListener(PlaceNavigationFragment.this);
            mLocationClient.start();
            Log.d("LocSDK3", "onPreExecute--done!!");
        }
    }
}
