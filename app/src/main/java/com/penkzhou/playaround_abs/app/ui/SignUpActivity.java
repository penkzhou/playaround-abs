package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SignUpCallback;
import com.penkzhou.playaround_abs.app.MainActivity;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.util.StringUtil;

public class SignUpActivity extends SherlockFragmentActivity implements View.OnClickListener {
    private Button signupButton;
    private EditText emailText;
    private EditText passwordText;
    private EditText usernameText;
    private String emailString;
    private String passwordString;
    private String usernameString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signupButton = (Button) findViewById(R.id.btn_signup_signup);
        signupButton.setOnClickListener(this);
        emailText = (EditText) findViewById(R.id.etSignupEmail);
        passwordText = (EditText) findViewById(R.id.etSignupPassword);
        usernameText = (EditText) findViewById(R.id.etSignupUsername);
    }


    @Override
    public void onClick(View v) {
        emailString = emailText.getText().toString();
        passwordString = passwordText.getText().toString();
        usernameString = usernameText.getText().toString();
        if (!(StringUtil.checkLoginAccount(usernameString) && StringUtil.checkLoginAccount(passwordString) && StringUtil.isNotBlank(emailString))) {
            Toast.makeText(this, getText(R.string.signup_stringcheck_nullemail), Toast.LENGTH_LONG).show();
            return;
        }
        if (!StringUtil.isValidEmail(emailString)) {
            Toast.makeText(this, getText(R.string.signup_stringcheck_wrongemail), Toast.LENGTH_LONG).show();
            return;
        }
        if (passwordString.length() < 6) {
            Toast.makeText(this, getText(R.string.signup_stringcheck_shortpwd), Toast.LENGTH_LONG).show();
            return;
        }
        AVUser user = new AVUser();
        user.setUsername(usernameString);
        user.setPassword(passwordString);
        user.setEmail(emailString);
        final ProgressDialog pdl = new ProgressDialog(SignUpActivity.this);
        pdl.setTitle(getText(R.string.signup_bar_title));
        pdl.setMessage(getText(R.string.signup_bar_msg));
        pdl.show();
        user.signUpInBackground(new SignUpCallback() {
            public void done(AVException e) {
                pdl.dismiss();
                if (e == null) {
                    Intent main = new Intent(SignUpActivity.this, MainActivity.class);
                    startActivity(main);
                    finish();
                } else {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("signup", e.toString());
                }
            }
        });
    }
}
