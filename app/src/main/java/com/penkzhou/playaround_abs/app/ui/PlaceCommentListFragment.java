package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround_abs.app.OnPlaceDataPass;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.PlaceCommentListAdapter;
import com.penkzhou.playaround_abs.app.model.ActionItem;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceComment;
import com.penkzhou.playaround_abs.app.util.StringUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by Administrator on 14-2-26.
 */
public class PlaceCommentListFragment extends SherlockFragment implements AdapterView.OnItemClickListener, OnRefreshListener, AbsListView.OnScrollListener, View.OnClickListener {


    private final static ArrayList<ActionItem> actionItems = new ArrayList<ActionItem>();
    private ListView placeCommentList;
    private TextView loadingTextView, nullTipTextView;
    private EditText commentEditView;
    private LinearLayout loadMoreView, loading, commentBox;
    private SharedPreferences preferences;
    private PullToRefreshLayout mPullToRefreshLayout;
    private boolean isRefresh = false;
    private String placeId;
    private int preLast;
    private PlaceComment pc;
    private Place currentPlace;
    private OnPlaceDataPass onPlaceDataPass;
    private List<PlaceComment> commentList;
    private ImageButton commentButton;
    private PlaceCommentListAdapter placeCommentListAdapter;
    private ProgressBar commentProgressBar;

    public PlaceCommentListFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        Log.d("ProfileFragment", "onAttach");
        super.onAttach(activity);
        try {
            onPlaceDataPass = (OnPlaceDataPass) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnPlaceDataPass");
        }
        placeId = onPlaceDataPass.getCurrentPlaceId();
    }

    @Override
    public void onRefreshStarted(View view) {
        isRefresh = true;
        placeCommentList.setVisibility(View.INVISIBLE);
        loadPlace();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_place_comments, container, false);
        isRefresh = false;
        mPullToRefreshLayout = (PullToRefreshLayout) rootView.findViewById(R.id.ptr_place_comment_layout);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ActionBarPullToRefresh.from(getActivity())
                .allChildrenArePullable()
                .listener(this)
                .setup(mPullToRefreshLayout);
        placeCommentList = (ListView) rootView.findViewById(R.id.lv_place_comment_list);
        placeCommentList.setOnScrollListener(this);
        placeCommentList.setOnItemClickListener(this);
        loadingTextView = (TextView) rootView.findViewById(R.id.tv_place_comment_loading);
        loadMoreView = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.listfooter_more_for_comment, null);
        loadMoreView.setVisibility(View.INVISIBLE);
        placeCommentList.addFooterView(loadMoreView);
        loading = (LinearLayout) rootView.findViewById(R.id.layout_place_comment_loading);
        nullTipTextView = (TextView) rootView.findViewById(R.id.tv_place_comment_nulltip);
        commentButton = (ImageButton) rootView.findViewById(R.id.btn_place_comment);
        commentButton.setOnClickListener(this);
        commentEditView = (EditText) rootView.findViewById(R.id.et_place_comment_content);
        commentProgressBar = (ProgressBar) rootView.findViewById(R.id.pb_place_comment);
        commentBox = (LinearLayout) rootView.findViewById(R.id.ll_place_commentbox);
        loadPlace();
        return rootView;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("onItemClick", " :" + position);
        switch (view.getId()) {
            case R.id.ll_place_comment_loadmore:
                Log.d("onItemClick", " :load more");
                break;
            default:
                final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
                final PlaceComment comment = (PlaceComment) parent.getAdapter().getItem(position);
                dialog.setTitle("评论操作");
                dialog.setCanceledOnTouchOutside(true);
                final View commentView = getActivity().getLayoutInflater().inflate(
                        R.layout.comment_action_list, null);
                final TextView replyComment = (TextView) commentView.findViewById(R.id.tv_comment_reply);
                final TextView viewCommentor = (TextView) commentView.findViewById(R.id.tv_comment_viewuser);
                replyComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Log.d("onItemClick", " :comment more" + comment.getObjectId());
                        commentEditView.setText("");
                        commentEditView.append("回复 " + "@" + comment.getSource().getUsername() + " :");
                        commentEditView.requestFocus();
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    }
                });
                viewCommentor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), ProfileActivity.class);
                        intent.putExtra("objectId", comment.getSource().getObjectId());
                        getActivity().startActivity(intent);
                    }
                });
                dialog.setView(commentView);
                dialog.show();
                break;
        }
    }

    public void loadPlace() {
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereEqualTo("objectId", placeId);
        query.limit(1);
        query.getFirstInBackground(new GetCallback<Place>() {
            @Override
            public void done(Place place, AVException e) {
                if (e == null) {
                    Log.d("成功", "loadPlace 查询到SinglePlace数据");
                    currentPlace = place;
                    loadCommentList();
                } else {
                    Log.d("失败", "LoadPlace 查询错误: " + e.getMessage());
                }
            }
        });
    }

    public void loadCommentList() {
        Log.d("loadCommentList", "------------- start----------");
        AVQuery<PlaceComment> query = new AVQuery<PlaceComment>("PlaceComment");
        query.whereEqualTo("place", currentPlace);
        query.include(PlaceComment.SOURCE);
        query.findInBackground(new FindCallback<PlaceComment>() {
            public void done(List<PlaceComment> avObjects, AVException e) {
                if (e == null) {
                    Log.d("成功", "loadCommentList 查询到" + avObjects.size() + " 条符合条件的数据");
                    //loadCommentProgressBar.setVisibility(View.GONE);
                    commentList = avObjects;
                    int commentNum = commentList.size();
                    if (commentNum == 0) {//说明还没有评论
                        nullTipTextView.setVisibility(View.VISIBLE);
                    } else {//加载评论
                        if (commentNum > 10) {
                            loadMoreView.setVisibility(View.VISIBLE);
                        }
                        placeCommentListAdapter = new PlaceCommentListAdapter(getActivity(), R.layout.place_comment_item, avObjects);
                        //
                        placeCommentList.setAdapter(placeCommentListAdapter);
                        placeCommentList.setOnItemClickListener(PlaceCommentListFragment.this);
                    }
                    loading.setVisibility(View.GONE);
                    commentBox.setVisibility(View.VISIBLE);
                } else {
                    Log.d("失败", "loadCommentList 查询错误: " + e.getMessage());
                    nullTipTextView.setVisibility(View.VISIBLE);
                }
                Log.d("loadCommentList", "------------- end ----------");
                forceCloseKeyboard();
            }
        });


    }

    public void forceCloseKeyboard() {
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

        //if (scrollState)
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        switch (view.getId()) {
            case R.id.lv_placelist_list:
                final int lastItem = firstVisibleItem + visibleItemCount;
                boolean isAutoload = preferences.getBoolean("autoload", true);
                if (isAutoload && lastItem == totalItemCount) {
                    if (preLast != lastItem) { //to avoid multiple calls for last item
                        Log.d("onScroll", "Last " + new Date().getTime());
                        preLast = lastItem;
                    }
                }
        }
    }

    @Override
    public void onClick(View v) {
        String commentContent = commentEditView.getText().toString();
        if (StringUtil.isBlank(commentContent)) {
            Toast.makeText(getActivity(), "评论不能为空", Toast.LENGTH_LONG).show();
            return;
        }
        commentProgressBar.setVisibility(View.VISIBLE);
        commentEditView.setVisibility(View.GONE);
        pc = new PlaceComment();
        pc.setContent(commentContent);
        pc.setPlace(currentPlace);
        pc.setSource(AVUser.getCurrentUser());
        currentPlace.increment(Place.PLACECOMMENTTIMES);
        pc.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    currentPlace.saveInBackground();
                    Log.d("成功", "添加评论成功");
                    commentProgressBar.setVisibility(View.GONE);
                    commentEditView.setText("");
                    commentEditView.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "添加评论成功", Toast.LENGTH_LONG).show();
                    if (nullTipTextView.getVisibility() == View.VISIBLE) {//说明还没有评论
                        nullTipTextView.setVisibility(View.GONE);
                        placeCommentListAdapter = new PlaceCommentListAdapter(getActivity(), R.layout.place_comment_item, new ArrayList<PlaceComment>());
                        placeCommentListAdapter.add(pc);
                        placeCommentList.setAdapter(placeCommentListAdapter);
                        placeCommentList.setOnItemClickListener(PlaceCommentListFragment.this);
                    } else {
                        placeCommentListAdapter.add(pc);
                        placeCommentListAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("失败", "(doInBackground)添加评论错误: " + e.getMessage());
                }
            }
        });
    }
}
