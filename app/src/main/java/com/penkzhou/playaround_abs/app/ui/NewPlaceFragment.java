package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.penkzhou.playaround_abs.app.MainActivity;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.IconTextAdapter;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceTypeItem;
import com.penkzhou.playaround_abs.app.util.StringUtil;

import java.util.ArrayList;

public class NewPlaceFragment extends SherlockFragment implements View.OnClickListener, BDLocationListener {

    private final static int[] typeIcons = {R.drawable.basketball, R.drawable.table_tennis,
            R.drawable.tennis, R.drawable.football, R.drawable.badminton, R.drawable.snooker,
            R.drawable.volleyball, R.drawable.running, R.drawable.swimming, R.drawable.climbing, R.drawable.rockclimbing};
    private static String[] typeNames = {};
    public LocationClient mLocationClient = null;
    MainActivity mParent;
    private Spinner typeSpinner;
    private Button sendButton;
    private TextView usernameTextView, locationText;
    private ProgressBar processBar;
    private EditText placeNameEditView, placeDescribeEditView;
    private BDLocation currentLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newplace, container, false);
        typeNames = getResources().getStringArray(R.array.newplace_list_type);
        usernameTextView = (TextView) rootView.findViewById(R.id.tv_newplace_username);
        usernameTextView.setText(AVUser.getCurrentUser().getUsername());
        placeNameEditView = (EditText) rootView.findViewById(R.id.et_newplace_name);
        placeDescribeEditView = (EditText) rootView.findViewById(R.id.et_newplace_describe);
        typeSpinner = (Spinner) rootView.findViewById(R.id.sp_newplace_type);
        locationText = (TextView) rootView.findViewById(R.id.tv_newplace_location);
        IconTextAdapter iconTextAdapter = new IconTextAdapter(getActivity(), android.R.layout.simple_spinner_item, getAllTypeItems());
        typeSpinner.setAdapter(iconTextAdapter);
        typeSpinner.setSelection(0);
        sendButton = (Button) rootView.findViewById(R.id.btn_newplace_send);
        sendButton.setOnClickListener(this);
        sendButton.setEnabled(false);
        processBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        startLocating();
        return rootView;
    }

    public void startLocating() {
        new LoadingLocationTask().execute();
    }

    private ArrayList<PlaceTypeItem> getAllTypeItems() {
        final ArrayList<PlaceTypeItem> allItems = new ArrayList<PlaceTypeItem>();
        for (int i = 0; i < typeNames.length; i++) {
            PlaceTypeItem placeTypeItem = new PlaceTypeItem();
            placeTypeItem.setTypeIconId(typeIcons[i]);
            placeTypeItem.setTypeName(typeNames[i]);
            allItems.add(placeTypeItem);
        }
        return allItems;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;
    }

    @Override
    public void onClick(View v) {
        int i = typeSpinner.getSelectedItemPosition();
        Place newPlace = new Place();
        AVGeoPoint geoPoint = new AVGeoPoint();
        geoPoint.setLatitude(currentLocation.getLatitude());
        geoPoint.setLongitude(currentLocation.getLongitude());
        newPlace.setPlaceLocation(geoPoint);
        String placeName = placeNameEditView.getText().toString();
        String placeDescribe = placeDescribeEditView.getText().toString();
        if (StringUtil.isBlank(placeName) || StringUtil.isBlank(placeDescribe)) {
            Toast.makeText(getActivity(), "你的输入不能为空", Toast.LENGTH_LONG).show();
            return;
        }
        newPlace.setPlaceName(placeName);
        newPlace.setPlaceDescribe(placeDescribe);
        newPlace.setPlaceOwner(AVUser.getCurrentUser());
        newPlace.setPlaceAddress(currentLocation.getAddrStr());
        newPlace.setPlaceVisitTimes(0);
        newPlace.setPlaceCommentTimes(0);
        newPlace.setPlaceLikeTimes(0);
        newPlace.setPlaceType(Place.PlaceType.values()[i]);
        final ProgressDialog pdl = new ProgressDialog(getActivity());
        pdl.setMessage(getText(R.string.newplace_bar_msg));
        pdl.show();
        newPlace.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                pdl.dismiss();
                if (e == null) {
                    Toast.makeText(getActivity(), "地点添加成功", Toast.LENGTH_LONG);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new PlaceListFragment()).addToBackStack(null).commit();
                } else {
                    Toast.makeText(getActivity(), "地点添加出错，请重新添加", Toast.LENGTH_LONG);
                }
            }
        });

    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        if (bdLocation != null) {
            currentLocation = bdLocation;
            locationText.setTag(bdLocation);
            mLocationClient.stop();
            processBar.setVisibility(View.GONE);
            locationText.setText(bdLocation.getAddrStr());
            sendButton.setEnabled(true);
        } else {
            Toast.makeText(getActivity(), "定位出现问题", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onReceivePoi(BDLocation bdLocation) {

    }

    private class LoadingLocationTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("LocSDK3", "doInBackground");
            if (mLocationClient != null && mLocationClient.isStarted())
                mLocationClient.requestLocation();
            else
                Log.d("LocSDK3", "locClient is null or not started");
            return null;
        }

        @Override
        protected void onPreExecute() {
            Log.d("LocSDK3", "onPreExecute");
            mLocationClient = new LocationClient(getActivity());
            LocationClientOption option = new LocationClientOption();
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
            option.setCoorType("bd09ll");//返回的定位结果是百度经纬度，默认值gcj02
            option.setScanSpan(5000);
            option.setIsNeedAddress(true);//返回的定位结果包含地址信息
            option.setNeedDeviceDirect(true);//返回的定位结果包含手机机头的方向
            mLocationClient.setLocOption(option);
            mLocationClient.registerLocationListener(NewPlaceFragment.this);

            mLocationClient.start();
            Log.d("LocSDK3", "onPreExecute--done!!");
        }
    }
}
