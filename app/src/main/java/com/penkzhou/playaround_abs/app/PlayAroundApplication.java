package com.penkzhou.playaround_abs.app;

import android.app.Application;
import android.graphics.Typeface;

import com.avos.avoscloud.AVAnalytics;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.PushService;
import com.baidu.location.BDLocation;
import com.penkzhou.playaround_abs.app.model.Feedback;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceComment;
import com.penkzhou.playaround_abs.app.model.PlaceOperation;

/**
 * Created by Administrator on 14-2-9.
 */
public class PlayAroundApplication extends Application {
    public static Typeface boldTF;
    public static Typeface lightTF;
    public static Typeface regularTF;
    public static BDLocation currentLocation;

    @Override
    public void onCreate() {
        super.onCreate();
        AVOSCloud.useAVCloudCN();
        AVObject.registerSubclass(Place.class);
        AVObject.registerSubclass(PlaceComment.class);
        AVObject.registerSubclass(PlaceOperation.class);
        AVObject.registerSubclass(Feedback.class);
        AVAnalytics.enableCrashReport(this, true);
        AVUser.enableAutomaticUser();
        AVInstallation.getCurrentInstallation().saveInBackground();
        PushService.setDefaultPushCallback(this, MainActivity.class);
        loadTypeFace();
    }


    public void loadTypeFace() {
        boldTF = Typeface.createFromAsset(getBaseContext().getAssets(), "Roboto-Bold.ttf");
        lightTF = Typeface.createFromAsset(getBaseContext().getAssets(), "Roboto-Light.ttf");
        regularTF = Typeface.createFromAsset(getBaseContext().getAssets(), "RobotoCondensed-Regular.ttf");
    }
}
