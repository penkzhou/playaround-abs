package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.google.analytics.tracking.android.EasyTracker;
import com.penkzhou.playaround_abs.app.MainActivity;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.util.StringUtil;

public class LoginActivity extends SherlockFragmentActivity implements View.OnClickListener {
    private Button loginButton, signupButton;
    private EditText usernameText, passwordText;
    private String usernameString, passwordString;
    private TextView forgetPassword;
    private SharedPreferences preferences;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void init() {
        loginButton = (Button) findViewById(R.id.btn_login_login);
        signupButton = (Button) findViewById(R.id.btn_login_signup);
        usernameText = (EditText) findViewById(R.id.etLoginUsername);
        passwordText = (EditText) findViewById(R.id.etLoginPassword);
        forgetPassword = (TextView) findViewById(R.id.tv_login_forget);
        loginButton.setOnClickListener(this);
        signupButton.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login_login:
                usernameString = usernameText.getText().toString();
                passwordString = passwordText.getText().toString();
                if (!(StringUtil.checkLoginAccount(usernameString) && StringUtil.checkLoginAccount(passwordString))) {
                    Toast.makeText(this, getText(R.string.login_stringcheck_username), Toast.LENGTH_LONG).show();
                    return;
                }
                final ProgressDialog pdl = new ProgressDialog(LoginActivity.this);
                pdl.setTitle(getText(R.string.login_bar_title));
                pdl.setMessage(getText(R.string.login_bar_msg));
                pdl.show();
                AVUser.logInInBackground(usernameString, passwordString, new LogInCallback() {
                    public void done(AVUser user, AVException e) {
                        pdl.dismiss();
                        if (e == null) {
                            if (user != null) {
                                // 登录成功
                                setDefaultPreference(user);
                                Intent main = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(main);
                                finish();
                            } else {
                                // 登录失败
                                Toast.makeText(LoginActivity.this, getText(R.string.login_failed_tip), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, getText(R.string.login_failed_tip), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            Log.e("LoginActivity", e.getMessage());
                        }
                    }
                });
                break;
            case R.id.btn_login_signup:
                Intent signup = new Intent(this, SignUpActivity.class);
                startActivity(signup);
                break;
            case R.id.tv_login_forget:
                Intent forget = new Intent(this, FindPasswordActivity.class);
                startActivity(forget);
                break;
        }
    }

    public void setDefaultPreference(AVUser user) {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", user.getUsername());
        editor.putString("email", user.getEmail());
        editor.putString("intro", user.getString("intro"));
        editor.commit();
    }


    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

}
