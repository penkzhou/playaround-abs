package com.penkzhou.playaround_abs.app.model;

/**
 * Created by Administrator on 14-2-21.
 */
public class PlaceTypeItem {
    private String typeName;
    private int typeIconId;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTypeIconId() {
        return typeIconId;
    }

    public void setTypeIconId(int typeIconId) {
        this.typeIconId = typeIconId;
    }
}
