package com.penkzhou.playaround_abs.app.adapter;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.util.StringUtil;

import java.util.List;

/**
 * Created by Administrator on 14-1-17.
 */
public class PlaceListAdapter extends ArrayAdapter<Place> {
    private final static int[] typeIcons = {R.drawable.basketball, R.drawable.table_tennis,
            R.drawable.tennis, R.drawable.football, R.drawable.badminton, R.drawable.snooker,
            R.drawable.volleyball, R.drawable.running, R.drawable.swimming, R.drawable.climbing, R.drawable.rockclimbing};
    private Context activity;
    private List<Place> objects;
    private BDLocation bdLocation;

    public PlaceListAdapter(Context context, int resource, List<Place> objects, BDLocation bdLocation) {
        super(context, resource, objects);
        this.activity = context;
        this.bdLocation = bdLocation;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.place_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.placeName = (TextView) v.findViewById(R.id.tv_placelist_name);
            viewHolder.placeAddress = (TextView) v.findViewById(R.id.tv_placelist_distance);
            viewHolder.placeDescribe = (TextView) v.findViewById(R.id.tv_placelist_desc);
            viewHolder.placeLikeTimes = (TextView) v.findViewById(R.id.tv_placelist_like);
            viewHolder.placeVisitTimes = (TextView) v.findViewById(R.id.tv_placelist_visit);
            viewHolder.placeCommentTimes = (TextView) v.findViewById(R.id.tv_placelist_comment);
            viewHolder.placeMeter = (TextView) v.findViewById(R.id.tv_placelist_meter);
            viewHolder.logo = (ImageView) v.findViewById(R.id.iv_placelist_logo);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final Place place = objects.get(position);

        if (place != null) {
            viewHolder.placeName.setText(place.getPlaceName());
            viewHolder.placeAddress.setText(place.getPlaceAddress());
            viewHolder.placeDescribe.setText(StringUtil.cutString(place.getPlaceDescribe(), 0, 10));
            viewHolder.placeLikeTimes.setText(String.valueOf(place.getPlaceLikeTimes()));
            viewHolder.placeVisitTimes.setText(String.valueOf(place.getPlaceVisitTimes()));
            viewHolder.placeCommentTimes.setText(String.valueOf(place.getPlaceCommentTimes()));
            Location source = new Location("A");
            source.setLatitude(bdLocation.getLatitude());
            source.setLongitude(bdLocation.getLongitude());
            Location destination = new Location("B");
            destination.setLatitude(place.getPlaceLocation().getLatitude());
            destination.setLongitude(place.getPlaceLocation().getLongitude());
            double distance = source.distanceTo(destination);
            String showMeter;
            distance = (double) (Math.round(distance * 10)) / 10;
            if (distance < 9999) {
                showMeter = "距您" + distance + "米";
            } else {
                distance = distance / 1000.0;
                distance = (double) (Math.round(distance * 10)) / 10;
                showMeter = "距您" + distance + "千米";
            }
            viewHolder.placeMeter.setText(showMeter);
            viewHolder.logo.setImageResource(typeIcons[place.getPlaceType().ordinal()]);
        }
        return v;
    }

    public static class ViewHolder {
        public TextView placeName, placeAddress, placeDescribe, placeLikeTimes, placeVisitTimes, placeCommentTimes, placeMeter;
        public ImageView logo;

    }
}
