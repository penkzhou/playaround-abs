package com.penkzhou.playaround_abs.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVFile;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.PlaceComment;
import com.penkzhou.playaround_abs.app.ui.ProfileActivity;
import com.penkzhou.playaround_abs.app.util.TimeUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 14-1-17.
 */
public class PlaceCommentListAdapter extends ArrayAdapter<PlaceComment> {
    private Context activity;
    private List<PlaceComment> objects;

    public PlaceCommentListAdapter(Context context, int resource, List<PlaceComment> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.place_comment_item, null);
            viewHolder = new ViewHolder();
            viewHolder.commentName = (TextView) v.findViewById(R.id.tv_place_comment_commentor);
            viewHolder.commentContent = (TextView) v.findViewById(R.id.tv_place_comment_commentcontent);
            viewHolder.commentTime = (TextView) v.findViewById(R.id.tv_place_comment_commenttime);
            viewHolder.avatar = (ImageView) v.findViewById(R.id.iv_place_comment_commentor);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        final PlaceComment comment = objects.get(position);
        final AVFile avatarFile = (AVFile) comment.getSource().get("avatarUrl");

        if (comment != null) {
            viewHolder.commentName.setText((comment.getSource()).getUsername());
            viewHolder.commentContent.setText(comment.getContent());
            viewHolder.commentTime.setText(TimeUtil.getTimeAgo(comment.getCreatedAt(), getContext()));
            if (avatarFile != null) {
                Picasso.with(getContext()).load(avatarFile.getUrl()).placeholder(R.drawable.no_avatar).error(R.drawable.no_avatar).into(viewHolder.avatar);
            } else {
                viewHolder.avatar.setImageResource(R.drawable.no_avatar);
            }
            viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ProfileActivity.class);
                    intent.putExtra("objectId", comment.getSource().getObjectId());
                    activity.startActivity(intent);
                }
            });
        }
        return v;
    }

    public static class ViewHolder {
        public TextView commentName, commentContent, commentTime;
        public ImageView avatar;

    }
}
