package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.RequestPasswordResetCallback;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.util.StringUtil;

public class FindPasswordActivity extends SherlockFragmentActivity implements View.OnClickListener {
    private Button findButton, gobackButton;
    private EditText emailText;
    private String emailString;
    private ProgressDialog pdl;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);
        init();
    }

    public void init() {
        findButton = (Button) findViewById(R.id.btn_login_find);
        gobackButton = (Button) findViewById(R.id.btn_login_goback);
        emailText = (EditText) findViewById(R.id.etForgetEmail);
        pdl = new ProgressDialog(FindPasswordActivity.this);
        findButton.setOnClickListener(this);
        gobackButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login_find:
                emailString = emailText.getText().toString();
                if (StringUtil.isBlank(emailString)) {
                    Toast.makeText(this, getText(R.string.login_stringcheck_nullemail), Toast.LENGTH_LONG).show();
                    return;
                }
                if (!StringUtil.isValidEmail(emailString)) {
                    Toast.makeText(this, getText(R.string.login_stringcheck_wrongemail), Toast.LENGTH_LONG).show();
                    return;
                }
                pdl.setTitle(getText(R.string.login_bar_title));
                pdl.setMessage(getText(R.string.login_bar_msg));
                pdl.show();
                AVUser.requestPasswordResetInBackground(emailString, new RequestPasswordResetCallback() {
                    @Override
                    public void done(AVException e) {
                        pdl.dismiss();
                        if (e == null) {
                            Toast.makeText(FindPasswordActivity.this, getText(R.string.login_forget_success_tip), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(FindPasswordActivity.this, getText(R.string.login_forget_failed_tip), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                break;
            case R.id.btn_login_goback:
                Intent login = new Intent(this, LoginActivity.class);
                startActivity(login);
                break;
        }
    }
}
