package com.penkzhou.playaround_abs.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.penkzhou.playaround_abs.app.PlayAroundApplication;
import com.penkzhou.playaround_abs.app.R;

/**
 * Created by Administrator on 14-1-17.
 */
public class MainMenuAdapter extends ArrayAdapter<String> {
    private Context activity;
    private String[] objects;

    public MainMenuAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.menu_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.menuText = (TextView) v.findViewById(R.id.tv_menu_item);
            viewHolder.menuText.setTypeface(PlayAroundApplication.lightTF);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final String menu = objects[position];

        if (menu != null) {
            viewHolder.menuText.setText(menu);
        }
        return v;
    }

    public static class ViewHolder {
        public TextView menuText;

    }
}
