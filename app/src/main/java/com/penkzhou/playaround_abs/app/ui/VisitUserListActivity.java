package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.UserListAdapter;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14-3-1.
 */
public class VisitUserListActivity extends SherlockFragmentActivity implements AdapterView.OnItemClickListener {

    private UserListAdapter userListAdapter;
    private ListView userList;
    private ProgressDialog pdl;
    private String visitPlaceId;
    private ActionBar mActionBar;
    private Place visitPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        visitPlaceId = getIntent().getStringExtra("visitPlaceId");
        setContentView(R.layout.activity_userlist);
        userList = (ListView) findViewById(R.id.lv_userlist_list);
        pdl = new ProgressDialog(this);
        pdl.setMessage(getString(R.string.loadstatus_bar_msg));
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setTitle(getString(R.string.visit_userlist_title));
        visitPlace = null;
        getVisitPlace();
    }


    public void getVisitPlace() {
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereEqualTo("objectId", visitPlaceId);
        query.getFirstInBackground(new GetCallback<Place>() {
            @Override
            public void done(Place place, AVException e) {
                if (e == null) {
                    visitPlace = place;
                    loadVisitUserList();
                } else {
                    Log.e("VisitUserListActivity--->getVisitPlace Failed", e.getMessage());
                }
            }
        });
    }

    public void loadVisitUserList() {
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, visitPlace);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
        query.include(PlaceOperation.SOURCE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            @Override
            public void done(List<PlaceOperation> placeOperations, AVException e) {
                if (e == null) {
                    ArrayList<AVUser> myUserList = new ArrayList<AVUser>();
                    for (PlaceOperation operation : placeOperations) {
                        myUserList.add(operation.getSource());
                    }
                    userListAdapter = new UserListAdapter(VisitUserListActivity.this, R.layout.user_list_item, myUserList);
                    userList.setAdapter(userListAdapter);
                    userList.setOnItemClickListener(VisitUserListActivity.this);
                    pdl.dismiss();
                } else {
                    Log.e("VisitUserListActivity--->loadVisitUserList", e.getMessage());
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AVUser user = userListAdapter.getItem(position);
        Intent intent = new Intent(VisitUserListActivity.this, ProfileActivity.class);
        intent.putExtra("objectId", user.getObjectId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }
}
