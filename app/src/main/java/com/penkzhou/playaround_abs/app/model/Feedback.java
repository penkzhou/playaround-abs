package com.penkzhou.playaround_abs.app.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;

/**
 * Created by Administrator on 14-2-21.
 */
@AVClassName("Feedback")
public class Feedback extends AVObject {

    public final static String CONTENT = "content";
    public final static String SOURCE = "source";
    public final static String OSVERSION = "osVersion";
    public final static String DEVICE = "device";
    public final static String SDK = "sdk";
    public final static String MODEL = "model";
    public final static String PRODUCT = "product";

    public Feedback() {
    }

    public String getContent() {
        return getString(CONTENT);
    }

    public void setContent(String content) {
        put(CONTENT, content);
    }

    public AVUser getSource() {
        return getAVUser(SOURCE);
    }

    public void setSource(AVUser source) {
        put(SOURCE, source);
    }


    public String getOsVersion() {
        return getString(OSVERSION);
    }

    public void setOsVersion(String content) {
        put(OSVERSION, content);
    }

    public String getDevice() {
        return getString(DEVICE);
    }

    public void setDevice(String content) {
        put(DEVICE, content);
    }

    public String getSDK() {
        return getString(SDK);
    }

    public void setSDK(String content) {
        put(SDK, content);
    }

    public String getModel() {
        return getString(MODEL);
    }

    public void setModel(String content) {
        put(MODEL, content);
    }

    public String getProduct() {
        return getString(PRODUCT);
    }

    public void setProduct(String content) {
        put(PRODUCT, content);
    }

}
