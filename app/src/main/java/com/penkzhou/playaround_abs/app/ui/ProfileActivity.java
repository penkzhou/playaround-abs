package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVAnalytics;
import com.google.analytics.tracking.android.EasyTracker;
import com.penkzhou.playaround_abs.app.OnProfileDataPass;

/**
 * Created by Administrator on 14-3-1.
 */
public class ProfileActivity extends SherlockFragmentActivity implements OnProfileDataPass {

    private ActionBar mActionBar;
    private String currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        currentUserId = intent.getStringExtra("objectId");
        init();
    }

    public void init() {
        mActionBar = getSupportActionBar();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(true);
        ActionBar.Tab tab = mActionBar.newTab()
                .setText("个人资料").setTabListener(new TabListener<ProfileFragment>(this, "profile", ProfileFragment.class));
        mActionBar.addTab(tab);
        tab = mActionBar.newTab()
                .setText("喜欢的地点").setTabListener(new TabListener<LikedPlaceListFragment>(this, "profile", LikedPlaceListFragment.class));
        mActionBar.addTab(tab);
        tab = mActionBar.newTab()
                .setText("去过的地点").setTabListener(new TabListener<VisitedPlaceListFragment>(this, "profile", VisitedPlaceListFragment.class));
        mActionBar.addTab(tab);
    }


    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    protected void onPause() {
        super.onPause();
        AVAnalytics.onPause(this);
    }

    protected void onResume() {
        super.onResume();
        AVAnalytics.onResume(this);
    }


    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void SetCurrentUserId(String userId) {
        this.currentUserId = userId;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private final Activity mActivity;
        private final String mTag;
        private final Class<T> mClass;
        private Fragment mFragment;

        /**
         * Constructor used each time a new tab is created.
         *
         * @param activity The host Activity, used to instantiate the fragment
         * @param tag      The identifier tag for the fragment
         * @param clz      The fragment's Class, used to instantiate the fragment
         */
        public TabListener(Activity activity, String tag, Class<T> clz) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
        }

    /* The following are each of the ActionBar.TabListener callbacks */

        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            // Check if the fragment is already initialized
            if (mFragment == null) {
                // If not, instantiate and add it to the activity
                mFragment = Fragment.instantiate(mActivity, mClass.getName());
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                // If it exists, simply attach it in order to show it
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                // Detach the fragment, because another one is being attached
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            // User selected the already selected tab. Usually do nothing.
        }


    }
}
