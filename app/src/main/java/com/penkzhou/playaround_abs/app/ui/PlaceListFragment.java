package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.penkzhou.playaround_abs.app.MainActivity;
import com.penkzhou.playaround_abs.app.PlayAroundApplication;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.adapter.PlaceListAdapter;
import com.penkzhou.playaround_abs.app.model.Place;

import java.util.Date;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by Administrator on 14-2-26.
 */
public class PlaceListFragment extends SherlockFragment implements AdapterView.OnItemClickListener, BDLocationListener, OnRefreshListener, AbsListView.OnScrollListener {


    MainActivity mParent;
    private PlaceListAdapter placeListAdapter;
    private ListView placeList;
    private TextView loadingTextView, nullTipTextView;
    private LinearLayout loadMoreView;
    private SharedPreferences preferences;
    private LinearLayout loading;
    private LocationClient mLocationClient = null;
    private BDLocation currentLocation, lastLocation;
    private AVGeoPoint geoPoint;
    private PullToRefreshLayout mPullToRefreshLayout;
    private boolean isRefresh = false;
    private boolean isLoading = true;
    private int distance, preLast;

    public PlaceListFragment() {

    }

    @Override
    public void onRefreshStarted(View view) {
        isRefresh = true;
        placeList.setVisibility(View.INVISIBLE);
        loadPlaceList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_placelist, container, false);
        isRefresh = false;
        mPullToRefreshLayout = (PullToRefreshLayout) rootView.findViewById(R.id.ptr_layout);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ActionBarPullToRefresh.from(getActivity())
                .allChildrenArePullable()
                .listener(this)
                .setup(mPullToRefreshLayout);
        placeList = (ListView) rootView.findViewById(R.id.lv_placelist_list);
        placeList.setOnScrollListener(this);
        loadingTextView = (TextView) rootView.findViewById(R.id.tv_placelist_loading);
        loadMoreView = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.listfooter_more, null);
        placeList.addFooterView(loadMoreView);
        loading = (LinearLayout) rootView.findViewById(R.id.layout_placelist_loading);
        nullTipTextView = (TextView) rootView.findViewById(R.id.tv_placelist_nulltip);
        new LoadingLocationTask().execute();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("onItemClick", " :" + position);
        switch (view.getId()) {
            case R.id.ll_placelist_loadmore:
                Log.d("onItemClick", " :load more");
                loadMore(5);
                break;
            default:
                Place choosePlace = placeListAdapter.getItem(position);
                goToDetail(choosePlace);
                break;
        }
    }

    public void goToDetail(Place place) {
        //Intent intent = new Intent(getActivity(), PlaceDetailActivity.class);
        Intent intent = new Intent(getActivity(), PlaceInfoActivity.class);
        intent.putExtra("placeId", place.getObjectId());
        startActivity(intent);
    }

    public void loadPlaceList() {
        geoPoint = new AVGeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereNear(Place.PLACELOCATION, geoPoint);
        //query.orderByAscending("createdAt");
        query.limit(5);
        query.findInBackground(new FindCallback<Place>() {
            public void done(List<Place> avObjects, AVException e) {
                if (e == null) {
                    if (avObjects.size() == 0) {
                        nullTipTextView.setVisibility(View.VISIBLE);
                    } else {
                        if (isRefresh) {
                            Log.d("mPullToRefreshLayout", "dd");
                            mPullToRefreshLayout.setRefreshComplete();
                            placeList.setVisibility(View.VISIBLE);
                        }
                        placeListAdapter = new PlaceListAdapter(getActivity(), R.layout.place_list_item, avObjects, currentLocation);
                        //
                        placeList.setAdapter(placeListAdapter);
                        placeList.setOnItemClickListener(PlaceListFragment.this);
                    }
                    loading.setVisibility(View.GONE);
                } else {
                    Log.d("PlaceListFragment", "loadPlaceList()失败,查询错误: " + e.getCode());
                    new AlertDialog.Builder(getActivity())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.placelist_jump)
                            .setMessage(R.string.placelist_goto_newplace)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Stop the activity
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.container, new NewPlaceFragment()).addToBackStack(null).commit();
                                }

                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                }
            }
        });
    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        if (bdLocation != null) {
            currentLocation = bdLocation;
            PlayAroundApplication.currentLocation = bdLocation;
            mLocationClient.stop();
            loadPlaceList();
        }
    }

    @Override
    public void onReceivePoi(BDLocation bdLocation) {

    }


    public void loadMore(int num) {
        loadMoreView.findViewById(R.id.pull_to_refresh_progress).setVisibility(View.VISIBLE);
        loadMoreView.findViewById(R.id.load_more).setVisibility(View.GONE);
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereNear(Place.PLACELOCATION, geoPoint);
        query.skip(placeList.getCount());
        query.limit(num);
        query.findInBackground(new FindCallback<Place>() {
            @Override
            public void done(List<Place> places, AVException e) {
                int placeNum = places.size();
                if (placeNum == 0) {
                    Toast.makeText(getActivity(), "没有更多了", Toast.LENGTH_SHORT).show();
                    loadMoreView.findViewById(R.id.load_more).setVisibility(View.GONE);
                } else {
                    for (int i = 0; i < placeNum; i++) {
                        placeListAdapter.add(places.get(i));
                    }
                    placeListAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), String.format(getString(R.string.placelist_load_num_tip), placeNum), Toast.LENGTH_SHORT).show();
                    //Crouton.makeText(getActivity(), String.format(getString(R.string.placelist_load_num_tip), placeNum), Style.INFO).show();
                    loadMoreView.findViewById(R.id.load_more).setVisibility(View.VISIBLE);
                }
                loadMoreView.findViewById(R.id.pull_to_refresh_progress).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

        //if (scrollState)
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        switch (view.getId()) {
            case R.id.lv_placelist_list:
                final int lastItem = firstVisibleItem + visibleItemCount;
                boolean isAutoload = preferences.getBoolean("autoload", true);
                if (isAutoload && lastItem == totalItemCount) {
                    if (preLast != lastItem) { //to avoid multiple calls for last item
                        Log.d("onScroll", "Last " + new Date().getTime());
                        preLast = lastItem;
                        loadMore(2);
                    }
                }
        }
    }

    private class LoadingLocationTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("LocSDK3", "doInBackground");
            if (mLocationClient != null && mLocationClient.isStarted())
                mLocationClient.requestLocation();
            else
                Log.d("LocSDK3", "locClient is null or not started");
            return null;
        }


        @Override
        protected void onPreExecute() {
            Log.d("LocSDK3", "onPreExecute");
            mLocationClient = new LocationClient(getActivity());
            loading.setVisibility(View.VISIBLE);
            LocationClientOption option = new LocationClientOption();
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
            option.setCoorType("bd09ll");//返回的定位结果是百度经纬度，默认值gcj02
            option.setScanSpan(5000);//设置发起定位请求的间隔时间为5000ms
            option.setIsNeedAddress(true);//返回的定位结果包含地址信息
            option.setNeedDeviceDirect(true);//返回的定位结果包含手机机头的方向
            mLocationClient.setLocOption(option);
            mLocationClient.registerLocationListener(PlaceListFragment.this);
            mLocationClient.start();
            distance = Integer.parseInt(preferences.getString(getString(R.string.preference_distance_key), "50"));

            Log.d("LocSDK3", "onPreExecute--done!!");
        }
    }
}
