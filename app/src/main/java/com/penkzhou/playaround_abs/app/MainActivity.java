package com.penkzhou.playaround_abs.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVAnalytics;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.feedback.FeedbackAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.penkzhou.playaround_abs.app.ui.ApplicationSettingActivity;
import com.penkzhou.playaround_abs.app.ui.LoginActivity;
import com.penkzhou.playaround_abs.app.ui.NewPlaceFragment;
import com.penkzhou.playaround_abs.app.ui.PlaceListFragment;
import com.penkzhou.playaround_abs.app.ui.PlaceListInMapActivity;
import com.penkzhou.playaround_abs.app.ui.ProfileActivity;


public class MainActivity extends SherlockFragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ActionBar actionBar;
    private FeedbackAgent agent;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        agent = new FeedbackAgent(this);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        actionBar = getSupportActionBar();
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        if (position == 2) {
            Intent gotoProfile = new Intent(this, ProfileActivity.class);
            gotoProfile.putExtra("objectId", AVUser.getCurrentUser().getObjectId());
            startActivity(gotoProfile);
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            onSectionAttached(position);
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new PlaceListFragment();
                    break;
                case 1:
                    fragment = new NewPlaceFragment();
                    break;
//                case 2:
//                    fragment = new StatusListFragment();
//                    break;
//                case 3:
//                    fragment = new NewPlaceFragment();
//                    break;
//                case 4:
//                    fragment = new PrivateTalkListFragment();
//                    break;
            }
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }
    }

    public void onSectionAttached(int number) {
        mTitle = getResources().getStringArray(R.array.main_menu)[number];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getSupportMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v("penkzhou_plus", "MainActivity.onOptionsItemSelected_" + item.getTitle().toString());
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_locate:
                Intent mapintent = new Intent(MainActivity.this, PlaceListInMapActivity.class);
                startActivity(mapintent);
                return true;
            case R.id.action_feedback:
                agent.startDefaultThreadActivity();
//                Intent feedback = new Intent(MainActivity.this, FeedbackActivity.class);
//                startActivity(feedback);
                return true;
            case R.id.action_settings:
                Intent setting = new Intent(MainActivity.this, ApplicationSettingActivity.class);
                startActivity(setting);
                return true;
            case R.id.action_logout:
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.quit)
                        .setMessage(R.string.really_quit)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Stop the activity
                                AVUser.logOut();
                                Log.d("currentUser_logout", AVUser.getCurrentUser().toString());
                                Intent login = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(login);
                                MainActivity.this.finish();
                            }

                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    public void onResume() {
        super.onResume();
        agent.sync();
        AVAnalytics.onResume(this);
    }


    public void onPause() {
        super.onPause();
        AVAnalytics.onPause(this);
    }


}
