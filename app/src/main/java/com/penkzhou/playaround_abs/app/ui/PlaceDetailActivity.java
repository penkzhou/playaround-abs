package com.penkzhou.playaround_abs.app.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround_abs.app.R;
import com.penkzhou.playaround_abs.app.model.Place;
import com.penkzhou.playaround_abs.app.model.PlaceComment;
import com.penkzhou.playaround_abs.app.model.PlaceOperation;
import com.penkzhou.playaround_abs.app.util.TimeUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 14-2-26.
 */
public class PlaceDetailActivity extends SherlockFragmentActivity implements View.OnClickListener {


    private final static int[] typeIcons = {R.drawable.basketball, R.drawable.table_tennis,
            R.drawable.tennis, R.drawable.football, R.drawable.badminton, R.drawable.snooker,
            R.drawable.volleyball, R.drawable.running, R.drawable.swimming, R.drawable.climbing, R.drawable.rockclimbing};
    private String placeId;
    private Place currentPlace;
    private List<PlaceComment> commentList;
    private LayoutInflater mLayoutInflater;
    private LinearLayout commentLayout, visitUserListLayout, likeUserListLayout;
    private TextView placeName, placeDescribe, placeAddress, placeVisitTimes, placeLikeTimes, placeWarning, visitWarning, likeWarning;
    private EditText commentEditView;
    private boolean isVisited = false;
    private boolean isLiked = false;
    private ProgressBar commentProgressBar, loadCommentProgressBar;
    private ImageView placeLogo, placeTopview;
    private Button commentButton;
    private ActionBar mActionBar;

    public void loadPlace() {
        Intent receiveIntent = getIntent();
        placeId = receiveIntent.getStringExtra("placeId");
        final ProgressDialog pdl = new ProgressDialog(PlaceDetailActivity.this);
        pdl.setMessage(getText(R.string.loadstatus_bar_msg));
        pdl.show();
        AVQuery<Place> query = new AVQuery<Place>("Place");
        query.whereEqualTo("objectId", placeId);
        query.limit(1);
        query.findInBackground(new FindCallback<Place>() {
            public void done(List<Place> avObjects, AVException e) {
                if (e == null) {
                    Log.d("成功", "loadPlace 查询到SinglePlace数据");
                    currentPlace = avObjects.get(0);
                    placeName.setText(currentPlace.getPlaceName());
                    placeDescribe.setText(currentPlace.getPlaceDescribe());
                    placeAddress.setText(currentPlace.getPlaceAddress());
                    placeLogo.setImageResource(typeIcons[currentPlace.getPlaceType().ordinal()]);
                    placeVisitTimes.setText(String.valueOf(currentPlace.getPlaceVisitTimes()));
                    placeLikeTimes.setText(String.valueOf(currentPlace.getPlaceLikeTimes()));
                    placeLikeTimes.setTag(true);
                    pdl.dismiss();
                    loadCommentList();
                    loadOperationLikeState();
                    loadOperationVisitState();
                    loadLikeList();
                    loadVisitList();
                } else {
                    Log.d("失败", "LoadPlace 查询错误: " + e.getMessage());
                }
            }
        });
    }

    public void loadOperationLikeState() {
        Log.d("loadOperationLikeState", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                if (e == null) {
                    Log.d("loadOperationLikeState 成功", "查询到" + i + " 条符合条件的数据");
                    if (i > 0) {
                        isLiked = true;
                        placeLikeTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_heart, 0, 0, 0);
                    } else {
                        isLiked = false;
                    }
                    placeLikeTimes.setTag(isLiked);
                } else {
                    Log.d("loadOperationLikeState 失败", e.getMessage());
                    placeLikeTimes.setTag(false);
                }
                Log.d("loadOperationLikeState", "------------- end----------");
            }
        });

    }

    public void loadOperationVisitState() {
        Log.d("loadOperationVisitState", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                if (e == null) {
                    Log.d("loadOperationVisitState成功", "查询到" + i + " 条符合条件的数据");
                    if (i > 0) {
                        isVisited = true;
                        placeVisitTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_location, 0, 0, 0);
                    } else {
                        isVisited = false;
                    }
                    placeVisitTimes.setTag(isVisited);
                } else {
                    Log.d("loadOperationVisitState失败", e.getMessage());
                    placeVisitTimes.setTag(false);
                }
                Log.d("loadOperationVisitState", "------------- end----------");
            }
        });
    }

    public void loadCommentList() {
        Log.d("loadCommentList", "------------- start----------");
        AVQuery<PlaceComment> query = new AVQuery<PlaceComment>("PlaceComment");
        query.whereEqualTo("place", currentPlace);
        query.include(PlaceComment.SOURCE);
        query.findInBackground(new FindCallback<PlaceComment>() {
            public void done(List<PlaceComment> avObjects, AVException e) {
                if (e == null) {
                    Log.d("成功", "loadCommentList 查询到" + avObjects.size() + " 条符合条件的数据");
                    loadCommentProgressBar.setVisibility(View.GONE);
                    commentList = avObjects;
                    if (commentList.size() == 0) {//说明还没有评论
                        placeWarning.setVisibility(View.VISIBLE);
                    } else {//加载评论
                        ArrayList<RelativeLayout> comments = new ArrayList<RelativeLayout>();
                        for (PlaceComment comment : commentList) {
                            RelativeLayout commentItem = (RelativeLayout) mLayoutInflater.inflate(R.layout.place_comment_item, null);
                            TextView commentName = (TextView) commentItem.findViewById(R.id.tv_place_comment_commentor);
                            TextView commentContent = (TextView) commentItem.findViewById(R.id.tv_place_comment_commentcontent);
                            ImageView commentAvatar = (ImageView) commentItem.findViewById(R.id.iv_place_comment_commentor);
                            TextView commentTime = (TextView) commentItem.findViewById(R.id.tv_place_comment_commenttime);
                            commentName.setText(comment.getSource().getUsername());
                            commentContent.setText(comment.getContent());
                            AVUser user = comment.getSource();
                            AVFile file = (AVFile) user.get("avatarUrl");
                            Picasso.with(PlaceDetailActivity.this).load(file.getUrl()).placeholder(R.drawable.no_avatar).error(R.drawable.no_avatar).into(commentAvatar);
                            commentTime.setText(TimeUtil.getTimeAgo(comment.getCreatedAt(), PlaceDetailActivity.this));
                            comments.add(commentItem);
                        }
                        for (RelativeLayout layout : comments) {
                            commentLayout.addView(layout, 0);
                        }
                    }
                } else {
                    Log.d("失败", "loadCommentList 查询错误: " + e.getMessage());
                    placeWarning.setVisibility(View.VISIBLE);
                }
                Log.d("loadCommentList", "------------- end ----------");
                forceCloseKeyboard();
            }
        });


    }

    public void loadVisitList() {
        Log.d("loadVisitList", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
        query.include(PlaceOperation.SOURCE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            @Override
            public void done(List<PlaceOperation> placeOperations, AVException e) {
                if (e == null) {
                    if (placeOperations.size() == 0) {
                        visitUserListLayout.setVisibility(View.GONE);
                    } else {
                        visitWarning.setVisibility(View.GONE);
                        for (PlaceOperation po : placeOperations) {
                            LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
                            final ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
                            AVUser user = po.getSource();
                            AVFile file = (AVFile) user.get("avatarUrl");
                            Picasso.with(getBaseContext()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
                            visitUserListLayout.addView(userItem);
                        }

                    }
                } else {
                    Log.e("失败", "loadVisitList 查询错误: " + e.getMessage());
                    visitUserListLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    public void loadLikeList() {
        Log.d("loadVisitList", "------------- start----------");
        AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
        query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
        query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
        query.include(PlaceOperation.SOURCE);
        query.findInBackground(new FindCallback<PlaceOperation>() {
            @Override
            public void done(List<PlaceOperation> placeOperations, AVException e) {
                if (e == null) {
                    if (placeOperations.size() == 0) {
                        likeUserListLayout.setVisibility(View.GONE);
                    } else {
                        likeWarning.setVisibility(View.GONE);
                        for (PlaceOperation po : placeOperations) {
                            LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
                            ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
                            AVUser user = po.getSource();
                            AVFile file = (AVFile) user.get("avatarUrl");
                            Picasso.with(getBaseContext()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
                            likeUserListLayout.addView(userItem);
                        }

                    }
                } else {
                    Log.e("失败", "loadVisitList 查询错误: " + e.getMessage());
                    likeUserListLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("成功", "PlaceDetailActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        init();
        loadPlace();
        forceCloseKeyboard();
    }

    public void forceCloseKeyboard() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void init() {
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        placeName = (TextView) findViewById(R.id.tv_placedetail_name);
        placeDescribe = (TextView) findViewById(R.id.tv_placedetail_describe);
        placeAddress = (TextView) findViewById(R.id.tv_placedetail_location);
        placeWarning = (TextView) findViewById(R.id.tv_placedetail_warning);
        placeLogo = (ImageView) findViewById(R.id.iv_placedetail_logo);
        placeTopview = (ImageView) findViewById(R.id.iv_placedetail_topview);
        placeVisitTimes = (TextView) findViewById(R.id.tv_placedetail_visit);
        placeLikeTimes = (TextView) findViewById(R.id.tv_placedetail_heart);
        commentButton = (Button) findViewById(R.id.btn_placedetail_comment);
        commentEditView = (EditText) findViewById(R.id.et_placedetail_comment);
        commentProgressBar = (ProgressBar) findViewById(R.id.pb_placedetail_comment);
        loadCommentProgressBar = (ProgressBar) findViewById(R.id.pb_placedetail_loadcomment);
        mLayoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        commentLayout = (LinearLayout) findViewById(R.id.linear_placedetail_commentlist);
        visitUserListLayout = (LinearLayout) findViewById(R.id.linear_placedetail_visitlist);
        likeUserListLayout = (LinearLayout) findViewById(R.id.linear_placedetail_likelist);
        visitWarning = (TextView) findViewById(R.id.warning_placedetail_visitlist);
        likeWarning = (TextView) findViewById(R.id.warning_placedetail_likelist);
        placeLikeTimes.setOnClickListener(this);
        placeVisitTimes.setOnClickListener(this);
        commentButton.setOnClickListener(this);
        likeUserListLayout.setOnClickListener(this);
        visitUserListLayout.setOnClickListener(this);
    }


    public void makeVisitOperation() {
        Log.d("makeVisitOperation", "------------- start----------");
        int currentVisitTimes = 0;
        if (!(Boolean) placeVisitTimes.getTag()) {
            placeVisitTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_location, 0, 0, 0);
            currentVisitTimes = Integer.valueOf(placeVisitTimes.getText().toString());
            placeVisitTimes.setText(String.valueOf(currentVisitTimes + 1));
            PlaceOperation po = new PlaceOperation();
            po.setSource(AVUser.getCurrentUser());
            po.setPlace(currentPlace);
            po.setOperationType(PlaceOperation.VISIT);
            currentPlace.increment(Place.PLACEVISITTIMES);
            po.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        isVisited = true;
                        placeVisitTimes.setTag(isVisited);
                        currentPlace.saveInBackground();
                        Log.d("PlaceOperation.VISTT", " 操作成功");
                        updateVisitList();
                        Toast.makeText(PlaceDetailActivity.this, "签到成功", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("PlaceOperation.VISTT", " 操作失败");
                    }
                    Log.d("makeVisitOperation", "------------- end----------");
                }
            });
        } else {
            Log.d("makeVisitOperation", "------------- else ----------");
            placeVisitTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_location_off, 0, 0, 0);
            currentVisitTimes = Integer.valueOf(placeVisitTimes.getText().toString());
            placeVisitTimes.setText(String.valueOf(currentVisitTimes - 1));
            AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
            query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
            query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
            query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.VISIT);
            currentPlace.increment(Place.PLACEVISITTIMES, -1);
            query.limit(1);
            currentPlace.saveInBackground();
            isVisited = false;
            placeVisitTimes.setTag(isVisited);
            query.findInBackground(new FindCallback<PlaceOperation>() {
                @Override
                public void done(List<PlaceOperation> placeOperations, AVException e) {
                    if (e == null) {
                        if (placeOperations.size() > 0) {
                            placeOperations.get(0).deleteInBackground();
                            Toast.makeText(PlaceDetailActivity.this, "您已取消签到", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d("makeVisitOperation error", "------------- end ----------");
                    }
                    Log.d("makeVisitOperation", "------------- end ----------");
                }
            });
        }
    }


    public void updateLikeList() {
        likeWarning.setVisibility(View.GONE);
        LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
        ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
        AVUser user = AVUser.getCurrentUser();
        AVFile file = (AVFile) user.get("avatarUrl");
        Picasso.with(getBaseContext()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
        likeUserListLayout.addView(userItem, 0);
    }


    public void updateVisitList() {
        visitWarning.setVisibility(View.GONE);
        LinearLayout userItem = (LinearLayout) mLayoutInflater.inflate(R.layout.avatar, null);
        ImageView avatar = (ImageView) userItem.findViewById(R.id.iv_avatar);
        AVUser user = AVUser.getCurrentUser();
        AVFile file = (AVFile) user.get("avatarUrl");
        Picasso.with(getBaseContext()).load(file.getUrl()).placeholder(R.drawable.no_avatar).into(avatar);
        visitUserListLayout.addView(userItem, 0);
    }

    public void makeLikeOperation() {

        Log.d("makeLikeOperation", "------------- start----------");
        int currentLikeTimes = 0;
        if (!(Boolean) placeLikeTimes.getTag()) {
            placeLikeTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_heart, 0, 0, 0);
            currentLikeTimes = Integer.valueOf(placeLikeTimes.getText().toString());
            placeLikeTimes.setText(String.valueOf(currentLikeTimes + 1));
            PlaceOperation po = new PlaceOperation();
            po.setSource(AVUser.getCurrentUser());
            po.setPlace(currentPlace);
            po.setOperationType(PlaceOperation.LIKE);
            currentPlace.increment(Place.PLACELIKETIMES);
            po.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        isLiked = true;
                        placeLikeTimes.setTag(isLiked);
                        currentPlace.saveInBackground();
                        updateLikeList();
                        Log.d("PlaceOperation.LIKE", " 操作成功");
                        Toast.makeText(PlaceDetailActivity.this, "收藏成功", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("PlaceOperation.LIKE", " 操作失败" + e.getCode());
                    }
                    Log.d("makeLikeOperation", "------------- end----------");
                }
            });
        } else {
            Log.d("makeLikeOperation", "------------- else ----------");
            placeLikeTimes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_heart_off, 0, 0, 0);
            currentLikeTimes = Integer.valueOf(placeLikeTimes.getText().toString());
            placeLikeTimes.setText(String.valueOf(currentLikeTimes - 1));
            AVQuery<PlaceOperation> query = new AVQuery<PlaceOperation>("PlaceOperation");
            query.whereEqualTo(PlaceOperation.PLACE, currentPlace);
            query.whereEqualTo(PlaceOperation.SOURCE, AVUser.getCurrentUser());
            query.whereEqualTo(PlaceOperation.OPERATION_TYPE, PlaceOperation.LIKE);
            currentPlace.increment(Place.PLACELIKETIMES, -1);
            query.limit(1);
            currentPlace.saveInBackground();
            isLiked = false;
            placeLikeTimes.setTag(isLiked);
            query.findInBackground(new FindCallback<PlaceOperation>() {
                @Override
                public void done(List<PlaceOperation> placeOperations, AVException e) {
                    if (e == null) {
                        if (placeOperations.size() > 0) {
                            placeOperations.get(0).deleteInBackground();
                            Toast.makeText(PlaceDetailActivity.this, "您已取消收藏", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d("makeLikeOperation error", "取消收藏失败 " + e.getCode());
                    }
                    Log.d("makeLikeOperation", "------------- end ----------");
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        currentPlace.setFetchWhenSave(true);
        switch (v.getId()) {
            case R.id.tv_placedetail_heart:
                makeLikeOperation();
                break;
            case R.id.tv_placedetail_visit:
                makeVisitOperation();
                break;
            case R.id.linear_placedetail_likelist:
                Intent toLike = new Intent(this, LikeUserListActivity.class);
                toLike.putExtra("likePlaceId", currentPlace.getObjectId());
                startActivity(toLike);
                break;
            case R.id.linear_placedetail_visitlist:
                Intent toVisit = new Intent(this, VisitUserListActivity.class);
                toVisit.putExtra("visitPlaceId", currentPlace.getObjectId());
                startActivity(toVisit);
                break;
            case R.id.btn_placedetail_comment:
                commentProgressBar.setVisibility(View.VISIBLE);
                commentEditView.setVisibility(View.GONE);
                PlaceComment pc = new PlaceComment();
                pc.setContent(commentEditView.getText().toString());
                pc.setPlace(currentPlace);
                pc.setSource(AVUser.getCurrentUser());
                currentPlace.increment(Place.PLACECOMMENTTIMES);
                pc.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e == null) {
                            currentPlace.saveInBackground();
                            placeWarning.setVisibility(View.GONE);
                            Log.d("成功", "添加评论成功");
                            commentProgressBar.setVisibility(View.GONE);
                            commentEditView.setText("");
                            commentEditView.setVisibility(View.VISIBLE);
                            Toast.makeText(PlaceDetailActivity.this, "添加评论成功", Toast.LENGTH_LONG).show();
                        } else {
                            Log.d("失败", "(doInBackground)添加评论错误: " + e.getMessage());
                        }
                    }
                });
                RelativeLayout commentItem = (RelativeLayout) mLayoutInflater.inflate(R.layout.place_comment_item, null);
                TextView commentName = (TextView) commentItem.findViewById(R.id.tv_place_comment_commentor);
                TextView commentContent = (TextView) commentItem.findViewById(R.id.tv_place_comment_commentcontent);
                ImageView commentAvatar = (ImageView) commentItem.findViewById(R.id.iv_place_comment_commentor);
                TextView commentTime = (TextView) commentItem.findViewById(R.id.tv_place_comment_commenttime);
                commentName.setText(pc.getSource().getUsername());
                commentContent.setText(pc.getContent());
                commentAvatar.setImageResource(R.drawable.no_avatar);
                commentTime.setText(getText(R.string.just_now));
                commentLayout.addView(commentItem, 0);
        }
    }
}
