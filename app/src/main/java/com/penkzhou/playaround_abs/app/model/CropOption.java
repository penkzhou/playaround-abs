package com.penkzhou.playaround_abs.app.model;

/**
 * Created by Administrator on 14-3-3.
 */

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
