package com.penkzhou.playaround_abs.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.avos.avoscloud.AVUser;
import com.google.analytics.tracking.android.EasyTracker;
import com.penkzhou.playaround_abs.app.MainActivity;

/**
 * Created by Administrator on 14-2-13.
 */
public class StartActivity extends Activity {
    private boolean isFirst;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFirst = getPreferences(MODE_PRIVATE).getBoolean("isFirst", true);
        if (isFirst) {
            isFirst = false;
            SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
            editor.putBoolean("isFirst", isFirst);
            editor.commit();
            firstStart();
        } else {
            normalStart();
        }

    }

    public void firstStart() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    normalStart();
                }
            }
        };
        timer.start();
    }

    public void normalStart() {
        if (AVUser.getCurrentUser().getObjectId().trim() == "") {
            Intent loginIntent = new Intent(StartActivity.this, LoginActivity.class);
            startActivity(loginIntent);
        } else {
            Intent mainIntent = new Intent(StartActivity.this, MainActivity.class);
            startActivity(mainIntent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
